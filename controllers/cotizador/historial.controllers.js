const Historial = require("../../models/cotizador/historial.model.js");

exports.addHistorial = (req, res)=>{
  if(!req.body){ // Validacion de request
	  res.status(400).send({
	    message:"El Contenido no puede estar vacio"
	  });
  }
  Historial.addHistorial(req.body, (err, data)=>{ // Guardar el CLiente en la BD
    
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el historial"
      })
    else res.send(data)
  });
};


// Find a single users with a usersId
exports.getHistorial = (req, res) => {
  Historial.getHistorial((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar el historial"
      });
    else res.send(data);
  });
};

