
const Users = require("../../models/cotizador/users.model.js");

exports.session = (req, res)=>{
  Users.login(req.body,(err, data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Este cliente no se encuentra registrado`
        });
      } else {
        res.status(500).send({
          message: "Error al buscar el usuario" 
        });
      }
    } else 
    console.log('data',data)
    res.send(data);
  });
};

exports.cambiaEstatus = (req, res) =>{
  Users.cambiaEstatus(req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el usuario con el id ${req.body.id }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el usuario con el id" + req.body.id 
        });
      }
    }else res.send("El usuario se actualizo correctamente.");
  })
};

// Find a single users with a usersId
exports.findOne = (req, res) => {
  Users.findById(req.params.userId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el cliente con el id ${ req.params.userId }.`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el usuario con el id" + req.params.userId 
        });
      }
    } else res.send(data);
  });
};

