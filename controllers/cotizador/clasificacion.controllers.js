
const Clasificacion = require("../../models/cotizador/clasificacion.model.js");

// Crear y salvar un nuevo usuario
exports.clasificacionAdd = (req, res) => {
  
  if(!req.body){ // Validacion de request
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }
  
  Clasificacion.clasificacionAdd(req.body, (err, data)=>{ // Guardar el CLiente en la BD
    
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el Usuario"
      })
    else res.send(data)

  })
};

// Traer todos los artículos
exports.clasificacionAll = (req, res) => { 
  Clasificacion.clasificacionAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};

// Traer la información de un solo artículo
exports.getClasificacion = (req, res) => {
  Clasificacion.getClasificacion(req.params.idcatego, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el cliente con el id ${ req.params.idcatego }.`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el usuario con el id" + req.params.idcatego 
        });
      }
    } else res.send(data);
  });
};


exports.updateClasificacion = (req, res) =>{
  Clasificacion.updateClasificacion(req.params.idcatego, req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el categoría con el id ${req.body.idcatego }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el artículo con el id" + req.body.idcatego 
        });
      }
    }else res.send("El artículo se actualizo correctamente.");
  })
};



// Delete a users with the specified usersId in the request
exports.deleteClasificacion = (req, res) => {
  Clasificacion.deleteClasificacion(req.params.idcatego, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found categoría with id ${req.params.idcatego}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el categoría con el id " + req.params.idcatego
        });
      }
    } else res.send({ message: `El categoría se elimino correctamente!` });
  });
};

// **************************************************************************
// **************************************************************************
// **************************************************************************

exports.clasartAdd = (req, res) => {
  
  if(!req.body){ // Validacion de request
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }
  
  Clasificacion.clasartAdd(req.body, (err, data)=>{ // Guardar el CLiente en la BD
    
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el Usuario"
      })
    else res.send(data)

  })
};

exports.deleteClasart = (req, res) => {
  Clasificacion.deleteClasart(req.params.idartcatego, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found categoría with id ${req.params.idartcatego}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el categoría con el id " + req.params.idartcatego
        });
      }
    } else res.send({ message: `El categoría se elimino correctamente!` });
  });
};


exports.unidadesAdd = (req, res) => {
  
  if(!req.body){ // Validacion de request
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }
  
  Clasificacion.unidadesAdd(req.body, (err, data)=>{ // Guardar el CLiente en la BD
    
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la unidad"
      })
    else res.send(data)

  })
};

// Traer todos los artículos
exports.unidadesAll = (req, res) => { 
  Clasificacion.unidadesAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar las unidades"
      });
    else res.send(data);
  });
};


exports.unidadesUpdate = (req, res) =>{
  Clasificacion.unidadesUpdate(req.params.idunidades, req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la unidad con el id ${req.body.idunidades }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar la unidad con el id" + req.body.idunidades 
        });
      }
    }else res.send("La unidad se actualizo correctamente.");
  })
};
