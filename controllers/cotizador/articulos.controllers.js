
const Articulos = require("../../models/cotizador/articulos.model.js");

// Crear y salvar un nuevo usuario
exports.articuloAdd = (req, res) => {
  
  if(!req.body){ // Validacion de request
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }
  
  Articulos.articuloAdd(req.body, (err, data)=>{ // Guardar el CLiente en la BD
    
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el Usuario"
      })
    else res.send(data)

  })
};

// Traer todos los artículos
exports.articulosAll = (req, res) => { 
  Articulos.articulosAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};

// Traer la información de un solo artículo
exports.getArticulo = (req, res) => {
  Articulos.getArticulo(req.params.idarticulo, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el cliente con el id ${ req.params.idarticulo }.`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el usuario con el id" + req.params.idarticulo 
        });
      }
    } else res.send(data);
  });
};


exports.updateArticulo = (req, res) =>{
  Articulos.updateArticulo(req.params.idarticulo, req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el articulo con el id ${req.body.idarticulo }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el artículo con el id" + req.body.idarticulo 
        });
      }
    }else res.send("El artículo se actualizo correctamente.");
  })
};



// Delete a users with the specified usersId in the request
exports.deleteArticulo = (req, res) => {
  Articulos.deleteArticulo(req.params.idarticulo, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found articulo with id ${req.params.idarticulo}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el articulo con el id " + req.params.idarticulo
        });
      }
    } else res.send({ message: `El articulo se elimino correctamente!` });
  });
};

// Delete a users with the specified usersId in the request
exports.deleteFoto = (req, res) => {
  Articulos.deleteFoto(req.body, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found foto with id ${req.body.idfoto}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre la foto con el id " + req.body.idfoto
        });
      }
    } else res.send({ message: `la foto se elimino correctamente!` });
  });
};

