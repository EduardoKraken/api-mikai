// Guardar imagenes
exports.addImagen = (req, res)=>{
  let EDFile = req.files.file
  EDFile.mv(`./../../fotos/${EDFile.name}`,err => {
      if(err) return res.status(500).send({ message : err })

      return res.status(200).send({ message : 'File upload' })
  })
};
