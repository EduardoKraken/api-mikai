
const Formatos = require("../../models/requisiciones/formatos.model.js");

// Crear un cliente
exports.addFormatos = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Formatos.addFormatos(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

exports.getFormatosID = (req, res)=>{
    Formatos.getFormatosID(req.body,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

exports.getAllFormatos = (req,res) => {
    Formatos.getAllFormatos((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};

exports.updateFormatos = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Formatos.updateFormatos(req.params.id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el cliente con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el cliente con el id" + req.params.id 
				});
			}
		} 
		else res.send(data);
	}
	);
}

exports.updateFormatoConsecutivo = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Formatos.updateFormatoConsecutivo(req.params.id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el cliente con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el cliente con el id" + req.params.id 
				});
			}
		} 
		else res.send(data);
	}
	);
}







