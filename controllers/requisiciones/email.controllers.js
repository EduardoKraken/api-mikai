var nodemailer  = require('nodemailer');
const newPDF    = require('../../helpers/generarPDF');

const fs = require('fs');

exports.enviarRequisicion = (req, res) => {

	newPDF( req.body.requisicion );

	const user = 'sofsolution.dev@gmail.com'
  const pass = 'ldeoykcqgsbhvxzc'

  // Preparamos el transporte
  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465, 
    secure:true,
    auth: {
      user,
      pass
    },
    from: user
  });

  var mailOptions = {
    from: '"MIKAI" <info@testsnode.com>',
    to:  `"${req.body.correo}"`,
    subject: `Requisiciones RQ-RC-CO-09-${ req.body.idrequisiciones }`,
    text: 'Hola, anexo requisición',
    attachments: [
    	{
    		filename: `RQ-RC-CO-09-${ req.body.idrequisiciones }.pdf`,
      	path    : `https://support-smipack.com/requisiciones-pdf/RQ-RC-CO-09-${ req.body.idrequisiciones }.pdf`
    	}
    ]
  };

  transporter.sendMail(mailOptions, (error, info) =>{
    if (error) {
    	console.log( error )
      res.status( 400 ).send({ message: 'Error al enviar el correo' })
    } else {
      console.log('Email sent: ' + info.response);
      res.send({ message: 'Correo enviado con éxito' })
    }
  });
}