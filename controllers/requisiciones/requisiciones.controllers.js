const requisiciones = require("../../models/requisiciones/requisiciones.model");

const newPDF        = require('../../helpers/generarPDF');

// Obtener el listado de horarios
exports.getRequisiciones = async(req, res) => {
  try {
  	// Consultar todas las requisicones
		const getRequisiciones = await requisiciones.getRequisiciones( ).then(response=> response)

		// Sacar el id de todas las requisiciones
		let idRequisiciones = getRequisiciones.map(( registro ) => { return registro.idrequisiciones })

		idRequisiciones = idRequisiciones.length ? idRequisiciones : [0]

		// Consultar todos los artículos de esas requisiciones
		const articulosRequi = await requisiciones.articulosRequi( idRequisiciones ).then(response=> response)

		// Consultar todos los proveedores de esas requisiciones
		const proveedoresRequi = await requisiciones.proveedoresRequi( idRequisiciones ).then(response=> response)

		// For para juntar toda la información
		for( const i in getRequisiciones ){

			const { idrequisiciones } = getRequisiciones[i]

			getRequisiciones[i]['articulos']   = articulosRequi.filter( el => { return el.idrequisiciones == idrequisiciones })
			getRequisiciones[i]['proveedores'] = proveedoresRequi.filter( el => { return el.idrequisiciones == idrequisiciones })
		}

		// Retornar toda la información
		res.send(getRequisiciones);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

// Obtener el listado de horarios
exports.getRequisicionesUsuario = async(req, res) => {
  try {

  	// Sacar el id del usuario
  	const { id } = req.params

  	// Consultar todas las requisicones
		const getRequisicionesUsuario = await requisiciones.getRequisicionesUsuario( id ).then(response=> response)

		// Sacar el id de todas las requisiciones
		let idRequisiciones = getRequisicionesUsuario.map(( registro ) => { return registro.idrequisiciones })

		idRequisiciones = idRequisiciones.length ? idRequisiciones : [0]

		// Consultar todos los artículos de esas requisiciones
		const articulosRequi = await requisiciones.articulosRequi( idRequisiciones ).then(response=> response)

		// Consultar todos los proveedores de esas requisiciones
		const proveedoresRequi = await requisiciones.proveedoresRequi( idRequisiciones ).then(response=> response)

		// For para juntar toda la información
		for( const i in getRequisicionesUsuario ){

			const { idrequisiciones } = getRequisicionesUsuario[i]

			getRequisicionesUsuario[i]['articulos']   = articulosRequi.filter( el => { return el.idrequisiciones == idrequisiciones })
			getRequisicionesUsuario[i]['proveedores'] = proveedoresRequi.filter( el => { return el.idrequisiciones == idrequisiciones })
		}

		// Retornar toda la información
		res.send(getRequisicionesUsuario);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getRequisicionesDepartamento = async(req, res) => {
  try {

  	// Sacar el id del usuario
  	const { id } = req.params

  	// Consultar todas las requisicones
		const getRequisicionesDepartamento = await requisiciones.getRequisicionesDepartamento( id ).then(response=> response)

		// Sacar el id de todas las requisiciones
		let idRequisiciones = getRequisicionesDepartamento.map(( registro ) => { return registro.idrequisiciones })

		idRequisiciones = idRequisiciones.length ? idRequisiciones : [0]

		// Consultar todos los artículos de esas requisiciones
		const articulosRequi = await requisiciones.articulosRequi( idRequisiciones ).then(response=> response)

		// Consultar todos los proveedores de esas requisiciones
		const proveedoresRequi = await requisiciones.proveedoresRequi( idRequisiciones ).then(response=> response)

		// For para juntar toda la información
		for( const i in getRequisicionesDepartamento ){

			const { idrequisiciones } = getRequisicionesDepartamento[i]

			getRequisicionesDepartamento[i]['articulos']   = articulosRequi.filter( el => { return el.idrequisiciones == idrequisiciones })
			getRequisicionesDepartamento[i]['proveedores'] = proveedoresRequi.filter( el => { return el.idrequisiciones == idrequisiciones })
		}

		// Retornar toda la información
		res.send(getRequisicionesDepartamento);

  } catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};

exports.addRequisiciones = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  const { articulos, proveedores, idsolicito, notas } = req.body

	  // Agregar la requisicion
	  const addRequisiciones = await requisiciones.addRequisiciones( req.body ).then(response=> response)

	  const { id } = addRequisiciones 

		// Agregar los articulos de esa requisiciones
		for( const i in articulos ){
	  	const addArticulosRequi   = await requisiciones.addArticulosRequi( articulos[i], id ).then(response=> response)
		}

	  // Agregar los proveedores de esa requisicion
		for( const i in proveedores ){
	  	const addProveedoresRequi = await requisiciones.addProveedoresRequi( proveedores[i], id ).then(response=> response)
		}


		if( notas ){
	  	const addNotasRequi = await requisiciones.addNotasRequi( idsolicito, notas, id ).then(response=> response)
		}


	  // Enviar el ciclo
	  res.send({ message: 'Requisicion generada correctamente' });

	} catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
};


exports.updateRequisciones = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos

	  if( !req.body.deleted ){
			req.body['deleted'] = 0
	  }

	  const updateRequisciones = await requisiciones.updateRequisciones( id, req.body ).then(response=> response)

	  res.send({message:'Datos actualizados correctamente' });

	} catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
}

exports.generarPDF = async (req, res) => {
	try {

		// GENERAR EL PDF
		newPDF( req.body );

		// Retornar toda la información
		res.send(req.body);

	} catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
}

exports.getNotasRequi = async (req, res) => {
	try {

		const { id } = req.params 

		const getNotasRequi = await requisiciones.getNotasRequi( id ).then( response => response )

		// Retornar toda la información
		res.send(getNotasRequi);

	} catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
}

exports.addNotasRequi = async (req, res) => {
	try {

		const { idusuario, notas, idrequisiciones } = req.body 

		const addNotasRequi = await requisiciones.addNotasRequi( idusuario, notas, idrequisiciones ).then(response=> response)

		// Retornar toda la información
		res.send({ message: 'Not agregada correctamente'});

	} catch (error) {
    res.status(500).send({message: error ? error.message : 'Error en el servidor' })
  }
}