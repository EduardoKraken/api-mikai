const departamentos = require("../../models/requisiciones/departamentos.model");

// Obtener el listado de horarios
exports.getDepartamentos = async(req, res) => {
  try {
  	// Consultar todos los ciclos
		const getDepartamentos = await departamentos.getDepartamentos( ).then(response=> response)
		// Enviar los ciclos
		res.send(getDepartamentos);

  } catch (error) {
    res.status(500).send({message:error})
  }
};

exports.addDepartamento = async (req, res) => {
	try {

	  //validamos que no venga vacio el body
	  if (!req.body || Object.keys(req.body).length === 0) {
	    return res.status(400).send({ message: "El Contenido no puede estar vacio" });
	  }

	  // Agregar el ciclo
	  const addDepartamento = await departamentos.addDepartamento( req.body ).then(response=> response)
	  // Enviar el ciclo
	  res.send( addDepartamento );

	} catch (error) {
    res.status(500).send({message:error})
  }
};


exports.updateDepartamento = async (req, res) => {
	try {
		const { id } = req.params
		// Validamos que no haya contenido vacio
	  if (!req.body || Object.keys(req.body).length === 0) {
	    res.status(400).send({ message: "El contenido no puede estar vacío" });
	  }
	  // Actualizamos
	  const updateDepartamento = await departamentos.updateDepartamento( id, req.body ).then(response=> response)

	  res.send({ updateDepartamento, message:'Ciclo actualizado correctamente' });

	} catch (error) {
    res.status(500).send({message:error})
  }
}
