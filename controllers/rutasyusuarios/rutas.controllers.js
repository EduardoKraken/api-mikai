
const Rutas = require("../../models/rutasyusuarios/rutas.model.js");

// Crear y salvar un nuevo usuario
exports.rutaAdd = (req, res) => {
  
  if(!req.body){ // Validacion de request
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }
  
  Rutas.rutaAdd(req.body, (err, data)=>{ // Guardar el CLiente en la BD
    
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el Usuario"
      })
    else res.send(data)

  })
};

// Traer todos los artículos
exports.rutasAll = (req, res) => { 
  Rutas.rutasAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};


exports.updateRuta = (req, res) =>{
  Rutas.updateRuta(req.params.idruta, req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el articulo con el id ${req.body.idruta }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el artículo con el id" + req.body.idruta 
        });
      }
    }else res.send("El artículo se actualizo correctamente.");
  })
};



