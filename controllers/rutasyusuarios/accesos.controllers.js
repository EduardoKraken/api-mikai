
const Accesos = require("../../models/rutasyusuarios/accesos.model.js");

// Crear y salvar un nuevo usuario
exports.accesosAdd = (req, res) => {
  
  if(!req.body){ // Validacion de request
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }
  
  Accesos.accesosAdd(req.body, (err, data)=>{ // Guardar el CLiente en la BD
    
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el acceso"
      })
    else res.send(data)

  })
};

// Traer todos los artículos
exports.accesosAll = (req, res) => { 
  Accesos.accesosAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los accesos"
      });
    else res.send(data);
  });
};





// Delete a users with the specified usersId in the request
exports.deleteAcceso = (req, res) => {
  Accesos.deleteAcceso(req.params.idacceso, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found acceso with id ${req.params.idacceso}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el acceso con el id " + req.params.idacceso
        });
      }
    } else res.send({ message: `El acceso se elimino correctamente!` });
  });
};

