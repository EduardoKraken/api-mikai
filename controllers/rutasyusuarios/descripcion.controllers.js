
const Descripcion = require("../../models/rutasyusuarios/descripcion.model.js");

// Crear y salvar un nuevo usuario
exports.descripcionAdd = (req, res) => {
  
  if(!req.body){ // Validacion de request
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }
  
  Descripcion.descripcionAdd(req.body, (err, data)=>{ // Guardar el CLiente en la BD
    
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el Usuario"
      })
    else res.send(data)

  })
};

// Traer todos los artículos
exports.descripcionAll = (req, res) => { 
  Descripcion.descripcionAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};

// Traer la información de un solo artículo
exports.getDescripcion = (req, res) => {
  Descripcion.getDescripcion(req.params.iddescripcion, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el cliente con el id ${ req.params.iddescripcion }.`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el usuario con el id" + req.params.iddescripcion 
        });
      }
    } else res.send(data);
  });
};


exports.updateDescripcion = (req, res) =>{
  Descripcion.updateDescripcion(req.params.iddescripcion, req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el categoría con el id ${req.body.iddescripcion }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el artículo con el id" + req.body.iddescripcion 
        });
      }
    }else res.send("El artículo se actualizo correctamente.");
  })
};



// Delete a users with the specified usersId in the request
exports.deleteDescripcion = (req, res) => {
  Descripcion.deleteDescripcion(req.params.iddescripcion, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found categoría with id ${req.params.iddescripcion}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el categoría con el id " + req.params.iddescripcion
        });
      }
    } else res.send({ message: `El categoría se elimino correctamente!` });
  });
};

