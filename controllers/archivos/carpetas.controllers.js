const carpetas = require("../../models/archivos/carpetas.model.js");
const { v4: uuidv4 } = require('uuid')


exports.getCarpetasNivel = async(req, res) => {
  try {

    const { nivel, idcarpetas } = req.body

    const carpetasNivel = await carpetas.getCarpetasNivel(nivel, idcarpetas).then(response => response)

    // const idCarpetasArchivos = carpetasNivel.map((registro) => { return registro.idcarpeta })

    const idCarpetasArchivos = [idcarpetas]
    
    let   archivos           = await carpetas.getArchivos(idCarpetasArchivos).then(response => response)

    return res.send({ carpetas: carpetasNivel, archivos })

  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};



exports.getCarpetasPrincipales = async(req, res) => {
  try {
    const carpetasIniciales = await carpetas.getCarpetasPrincipales(req.params.id).then(response => response);
    const archivos = await carpetas.getArchivosPrincipales().then(response => response)
    return res.send({ carpetas: carpetasIniciales, archivos })

  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};



exports.getCarpetaExacta = async(req, res) => {
  try {

    const { idcarpetas, idcarpeta } = req.body

    const carpetasIniciales = await carpetas.getCarpetaExacta(idcarpetas).then(response => response)

    const idCarpetasArchivos = [idcarpeta]
    
    let   archivos           = await carpetas.getArchivos(idCarpetasArchivos).then(response => response)

    return res.send({ carpetas: carpetasIniciales, archivos })

  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.agregarCarpeta = async(req, res) => {
  try {

    const agregarCarpeta = await carpetas.agregarCarpeta(req.body).then(response => response)
    const carpetaAgregada = await carpetas.getCarpeta(agregarCarpeta.id).then(response => response)

    return res.send(carpetaAgregada)

  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.subirArchivo = async(req, res) => {
  try {

    console.log( req.files )
    console.log( req.file )
    console.log( req.body )

    if (!req.files) {
      return res.status(400).send({ message: 'El contenido no puede ir vacio' })
    }

        // desestrucutramos los arvhios cargados
    const { files } = req.files
    const { idcarpetas } = req.body
    const { idusuario } = req.body

    const nombreSplit = files.name.split('.')

    const nombreArchivo = nombreSplit[0]

        // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[nombreSplit.length - 1]

        // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo

        // extensiones validas
    let extensiones = ['BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'svg', 'pdf', 'doc', 'docx', 'PDF']
    let ruta = '../../archivos-mikai/' + nombreUuid

        // validar que la extensión del archivo recibido sea valido
    if (!extensiones.includes(extensioArchivo))
      return res.status(400).send({ message: `Hola, la estensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }` })


    files.mv(`${ruta}`, async(err) => {
      if (err)
        return res.status(400).send({ message: err })
    })

    const subirImagen = await carpetas.agregarArchivo(nombreUuid, nombreArchivo, extensioArchivo, idcarpetas, idusuario).then(response => response)
    return res.send(subirImagen)

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.carpetasDisponibles = async(req, res) => {
  try {
    const carpetas_disponibles = await carpetas.carpetasDisponibles().then(response => response)
    return res.send(carpetas_disponibles)

  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.agregarAcceso = async(req, res) => {
  try {
    if (!req.body) {
      return res.status(500).send({ message: 'El contenido no puede ir vacio' })
    }

    const acceso = await carpetas.agregarAcceso(req.body).then(response => response)
    return res.send(acceso);
  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.eliminarAcceso = async(req, res) => {
  try {
    if (!req.params.id) {
      return res.status(500).send({ message: 'El contenido no puede ir vacio' })
    }

    const eliminar_acceso = await carpetas.eliminarAcceso(req.params.id).then(response => response)
    return res.send(eliminar_acceso);
  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.renombrarCarpeta = async(req, res) => {
  try {
    if (!req.body) {
      return res.status(500).send({ message: 'El contenido no puede ir vacio' })
    }

    const renombrar = await carpetas.renombrarCarpeta(req.body).then(response => response)
    return res.send(renombrar);
  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.eliminarCarpeta = async(req, res) => {
  try {
    if (!req.params.id) {
      return res.status(500).send({ message: 'El contenido no puede ir vacio' })
    }

    const eliminar_carpeta = await carpetas.eliminarCarpeta(req.params.id).then(response => response)
    return res.send(eliminar_carpeta);
  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.eliminarArchivo = async(req, res) => {
  try {
    if (!req.params.id) {
      return res.status(500).send({ message: 'El contenido no puede ir vacio' })
    }

    const eliminar_carpeta = await carpetas.eliminarArchivo(req.params.id).then(response => response)
    return res.send(eliminar_carpeta);
  } catch (error) {
    return res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
