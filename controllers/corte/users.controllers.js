
const Users = require("../../models/corte/models/users.model.js");

exports.session = (req, res)=>{
  Users.login(req.body,(err, data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Este cliente no se encuentra registrado`
        });
      } else {
        res.status(500).send({
          message: "Error al buscar el usuario" 
        });
      }
    } else 
    console.log('data',data)
    res.send(data);
  });
};

exports.registro = (req, res)=>{
  Users.validaEmail(req.body,(err,data)=>{
    if(data.length){ 
      res.status(200).send({
        message: `Ups, al parecer esté email ya se encuentra registrado.`
      });
    }else{
      Users.crearUsuario(req.body,(err,data)=>{ 
        if(err) 
          res.status(500).send({ message: err.message || "Se produjo algún error al crear el Usuario"
        })
         else res.send(data)
      })
    }
  });
}

exports.cambiaEstatus = (req, res) =>{
  Users.cambiaEstatus(req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el usuario con el id ${req.body.id }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el usuario con el id" + req.body.id 
        });
      }
    }else res.send("El usuario se actualizo correctamente.");
  })
};

// PROCESO PARA ACTUALIZAR PERFIL
exports.UpdateUsuario = (req, res) =>{
  Users.updateUsuario(req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el usuario con el id ${req.body.id }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el usuario con el id" + req.body.id 
        });
      }
    }else res.send(data);
  })
};

exports.updatexId = (req, res) =>{
  Users.updatexId(req.params.id, req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el usuario con el id ${req.params.id }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el usuario con el id" + req.params.id 
        });
      }
    }else res.send(data);
  })
};

// Crear y salvar un nuevo usuario
exports.create = (req, res) => {
	
  if(!req.body){ // Validacion de request
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }
  
  // const user = new User({ // Crear un usuario
  // 	email     : req.body.email,
  //   nomuser   : req.body.nomuser,
  // 	nomper    : req.body.nomper,
  //   nivel     : req.body.nivel,
  // 	password  : req.body.password,
  //   estatus   : req.body.estatus
  // })
  
  Users.create(req.body, (err, data)=>{ // Guardar el CLiente en la BD
  	
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Usuario"
  		})
  	else res.send(data)

  })
};

exports.findAll = (req, res) => { // Retrieve all userss from the database.
  Users.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};

// Find a single users with a usersId
exports.findOne = (req, res) => {
  Users.findById(req.params.userId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el cliente con el id ${ req.params.userId }.`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el usuario con el id" + req.params.userId 
        });
      }
    } else res.send(data);
  });
};

exports.OlvideContra = (req, res) => {
  Users.OlvideContra(req.body,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(202).send({
          message: `No encontre el correo`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el correo" 
        });
      }
    } else res.send(data);
  });
};

exports.passwordExtra = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }

  Users.passwordExtra(req.body,(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({
            message: `No encontre el usuario con el id ${req.body.id }.`
          });
        } else {
          res.status(500).send({
            message: "Error al actualizar el usuario con el id" + req.body.id 
          });
        }
      } 
      else res.send(data);
    }
  );
};


// Find a single users with a usersId
exports.getxEmail = (req, res) => {
  Users.getxEmail(req.body,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(202).send({
          message: `No encontre el correo`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el correo" 
        });
      }
    } else res.send(data);
  });
};

// Update a users identified by the usersId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }

  Users.updateById(req.params.userId, new User(req.body),(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({
            message: `No encontre el usuario con el id ${req.params.userId }.`
          });
        } else {
          res.status(500).send({
            message: "Error al actualizar el usuario con el id" + req.params.userId 
          });
        }
      } 
      else res.send(data);
    }
  );
};

// Delete a users with the specified usersId in the request
exports.delete = (req, res) => {
  Users.remove(req.params.usersId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.usersId}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.usersId
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};

// Delete all userss from the database.
exports.deleteAll = (req, res) => {
  Users.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al eliminar a todos los clientes."
      });
    else res.send({ message: `Todos los usuarios se eliminarion correctamente!` });
  });
};


exports.activarUsuario = (req, res) => {
  Users.activarUsuario(req.params.idusuario,(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `No encontre el usuario con el id ${req.params.idusuario }.`
          });
        } else {
          res.status(500).send({
            message: "Error al actualizar el usuario con el id" + req.params.idusuario 
          });
        }
      } 
      else res.send(data);
  });
};

exports.deleteUsuario = (req, res) => {
  Users.deleteUsuario(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found usuario with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};
