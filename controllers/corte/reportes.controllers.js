
const Reportes = require("../../models/corte/reportes.model.js");

exports.cierreDetalle = (req, res)=>{
  Reportes.cierreDetalle(req.body,(err, data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Este cliente no se encuentra registrado`
        });
      } else {
        res.status(500).send({
          message: "Error al buscar el usuario" 
        });
      }
    } else 
    console.log('data',data)
    res.send(data);
  });
};

exports.cierreDetalleFecha = (req, res)=>{
  Reportes.cierreDetalleFecha(req.body,(err, data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Este cliente no se encuentra registrado`
        });
      } else {
        res.status(500).send({
          message: "Error al buscar el usuario" 
        });
      }
    } else 
    console.log('data',data)
    res.send(data);
  });
};


exports.userCaja = (req, res) => { // Retrieve all userss from the database.
  Reportes.userCaja((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};

exports.saldoInicial = (req, res) => { // Retrieve all userss from the database.
  Reportes.saldoInicial(req.body.fecha,(err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};

exports.retiros = (req, res) => { // Retrieve all userss from the database.
  Reportes.retiros(req.params.idcaja,(err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};


