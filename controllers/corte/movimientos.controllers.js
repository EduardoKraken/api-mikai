
const Movimientos = require("../../models/corte/movimientos.model.js");
const { response } = require("express");

/*********************************************************** => MOVIMIENTOS            */ 
exports.Movimientos = (req, res) => {
  Movimientos.Movimientos(req.params.id, (err, response) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre la caja ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre la caja " + req.params.id
        });
      }
    } else res.status(200).send(response);
  });
};

exports.MovimientosFecha = (req, res) => {
  Movimientos.MovimientosFecha(req.body.fecha, (err, response) => {
    if(!req.body){ 
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
    } else res.status(200).send(response);
  });
};
/*********************************************************** => ADD MOVIMIENTO         */
exports.addMovimiento = (req, res) => {
  if(!req.body){ 
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Movimientos.addMovimiento(req.body, (err, response )=>{ 
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({ message: err.message || "Se produjo algún error al recuperar los movimientos"  })
    else 
      response ? res.status(200).send(response):
                 res.status(404).send({ message:'Error al crear movimeinto'})
  })
};
/*********************************************************** => PUT MOVIMIENTO         */
exports.putMovimiento = (req, res) =>{
  Movimientos.putMovimiento(req.params.id, req.body, (err,response)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `Ocurrio un error, intentelo mas tarde.` });
      } else {
        res.status(500).send({ message: "Error al actualizar el movimiento"  });
      }
    }else 
    response ? res.status(200).send({ mensaje: "El movimiento se actualizó correctamente.", body: response }):
               res.status(404).send({ message:'Error al modificar el movimeinto'});
  })
};
/*********************************************************** => PUT MOVIMIENTO         */
exports.cancelacion = (req, res) =>{
  Movimientos.cancelacion(req.params.id, req.body, (err,response)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `Ocurrio un error, intentelo mas tarde.` });
      } else {
        res.status(500).send({ message: "Error al cancelar el movimiento"  });
      }
    }else 
    response ? res.status(200).send({ mensaje: "La cancelacion se registro correctamente.", body: response }):
               res.status(404).send({ message:'Error al cancelar el movimeinto'});
  })
};
/*********************************************************** => PUT MOVIMIENTO         */
exports.devolucion = (req, res) =>{
  Movimientos.devolucion(req.params.id, req.body, (err,response)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({ message: `Ocurrio un error, intentelo mas tarde.` });
      } else {
        res.status(500).send({ message: "Error al generar devolución"  });
      }
    }else 
    response ? res.status(200).send({ mensaje: "La devolución se registro correctamente.", body: response }):
               res.status(404).send({ message:'Error al generar devolución'});
  })
};
/*********************************************************** => PUT MOVIMIENTO         */
exports.salida = (req, res) => {
  if(!req.body){ 
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Movimientos.salida(req.body, (err, response )=>{ 
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al recuperar la salida"
  		})
    else 
      response? res.status(200).send(response):
                res.status(404).send({ message:"No se logro generar la salida."})
  })
};

/*********************************************************** => GENERAR CORTE          */
exports.generaCorte = (req, res) => {
  if(!req.body){ 
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Movimientos.generaCorte(req.body, (err, response )=>{ 
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({ message: err.message || "Se produjo algún error al recuperar la información"  })
    else 
      response ? res.status(200).send(response):
                 res.status(404).send({ message:'Error al generar corte de caja.'})
  })
};
/*********************************************************** => RETIROS                */ 
exports.retiros = (req, res) => {
  Movimientos.Retiros(req.params.id, (err, response) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No se encontro el retiro con la caja ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No se encontro ningun retiro. " + req.params.id
        });
      }
    } else res.status(200).send(response);
  });
};








