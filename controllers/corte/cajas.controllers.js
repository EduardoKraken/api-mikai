
const Cajas = require("../../models/corte/cajas.model.js");

/*********************************************************** => ABRIR CAJA           */ 
exports.abrirCaja = (req, res) => {
  if(!req.body){ 
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Cajas.abrirCaja(req.body, (err, response )=>{ 
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al recuperar la caja"
  		})
    else 
      response? res.status(200).send(response):
                res.status(404).send({ message:"No se logro insertar el usuario"})
  })
};
/*********************************************************** => CAJA ABIERTA         */
exports.cajaAbierta = (req, res) => {
  if(!req.body){ 
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Cajas.cajaAbierta(req.body, (err, response )=>{ 
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al recuperar la caja"
  		})
    else 
      response.length ? res.status(200).send(response):
                        res.status(404).send({ message:'Este usuario no tiene caja abierta'})
  })
};
/*********************************************************** => DENOMINACIONES       */
exports.Denominaciones = (req, res) => {
  if(!req.body){ 
  	res.status(400).send({ message:"El Contenido no puede estar vacio"});
  }

  console.log(req.body)
  Cajas.Denominaciones(req.body, (err, response )=>{ 
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al recuperar la caja"
      })
    else 
      response ? res.status(200).send({ message:"Denominaciones creadas correctamente"}):
                 res.status(404).send({ message:"Ocurrio un error"})
  })
};
/*********************************************************** => AGREGAR ESTATUS CAJA */
exports.addEstatusCaja = (req, res) => {
  if(!req.body){ 
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Cajas.addEstatusCaja(req.body, (err, response )=>{ 
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al recuperar la caja"
  		})
    else 
      response ? res.status(200).send({ message:"Caja abierta correctamente."}):
                 res.status(404).send({ message:'Error al activar caja.'})
  })
};

/********************************************************* => obtener los datos de la caja */

exports.getCaja = (req, res) => { // Retrieve all userss from the database.
  Cajas.getCaja(req.params.idcaja, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};

/*********************************************************** => CERRAR CAJA           */ 
exports.cerrarCaja = (req, res) => {
  if(!req.body){ 
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Cajas.cerrarCaja(req.body, (err, response )=>{ 
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al recuperar la caja"
  		})
    else 
      response? res.status(200).send(response):
                res.status(404).send({ message:"No se logro cerrar la caja"})
  })
};

/*********************************************************** => DETALLE CERRAR CAJA   */ 
exports.detCierre = (req, res) => {
  if(!req.body){ 
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }
  let error = 0, errores = [];

  for(const i in req.body){
    Cajas.detCierre(req.body[i], (err, response )=>{ 
      if(err) { error ++; errores.push(req.body[i])  }
    })
  }

  if(error > 0){ 
    res.status(500).send({ message:"Ocurrio un error al crear el detalle", error: errores})
  }else{
    res.status(200).send({ message:"La caja se cerro correctamente"});
  }

};

exports.getCajasDia = (req, res) => {
  if(!req.body){ 
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  Cajas.getCajasDia(req.body.fecha, (err, response )=>{ 
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al recuperar la caja"
      })
    else 
      response.length ? res.status(200).send(response):
                        res.status(500).send({ message:'No hay cajas abiertas'})
  })
};

exports.deleteCierre = (req, res) => {
  if(!req.body){ 
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  Cajas.deleteCierre(req.body, (err, response )=>{ 
    if(err) // EVALUO QUE NO EXISTA UN ERROR
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al recuperar la caja"
      })
    else 
      res.status(200).send({ message:"La caja se cerro correctamente"});
  })
};










