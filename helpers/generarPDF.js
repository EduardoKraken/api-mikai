const PDFDocument = require('pdfkit');

const fs = require('fs');

const newPDF = ({
  idrequisiciones,
  solicito,
  usuario,
  departamento,
  aprobo,
  articulos,
  proveedores
}) => {

  // Crear un nuevo documento PDF
  const doc = new PDFDocument();

  // Nombre del y lugar del documento con número de folio

  if ( idrequisiciones <= 267 ){
  	doc.pipe(fs.createWriteStream(`../../requisiciones-pdf/RQ-RC-CO-09-${ idrequisiciones }.pdf`));
  }else{
  	doc.pipe(fs.createWriteStream(`../../requisiciones-pdf/RC-CO-01-${ idrequisiciones }.pdf`));
  }

  doc.image('../../requisiciones-pdf/mikai-logo-2.png', 65, 10, {width: 145});


  if ( idrequisiciones <= 267 ){
	  doc.fontSize(11).text( `RC-CO-01` , 450, 25, { align: 'left' });
	  doc.fontSize(11).text( `Rev. 0-07/21` , 450, 40, { align: 'left' });
  }else{
  	doc.fontSize(11).text( `RC-CO-01` , 450, 25, { align: 'left' });
	  doc.fontSize(11).text( `Rev. 0-08/24` , 450, 40, { align: 'left' });
	  doc.fontSize(11).text( `Vigencia: 08/27` , 450, 55, { align: 'left' });
  }



  doc.fontSize(16).text( `REQUISICIÓN DE COMPRA` , 65, 80, { align: 'center' });
  doc.moveDown();
  doc.moveDown();
  
  doc.fontSize(11).text( `N° Requisición RQ-${idrequisiciones}` , 65, 120, { align: 'right' });
  doc.moveDown();

  doc.fontSize(11).text( `Departamento Solicitante: ${ departamento }` , 65, 120, { align: 'left' });
  doc.moveDown();

  doc.fontSize(11).text( `Solicitado por: ${ usuario }` , { align: 'left' });
  doc.moveDown();

  doc.fontSize(8).text(`Partida`, 70, 180 ,{
	  width: 30,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Cant.`, 100, 180 , {
	  width: 40,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Unidad`, 140, 180 , {
	  width: 45,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Descripción`, 185, 180 , {
	  width: 250,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Orden. Compra`, 435, 180 , {
	  width: 250,
	  align: 'justify'
	})

  let inicio = 75
  let final  = 200

  for (const requisicionItem of articulos) {

  	doc.fontSize(10).text(`${requisicionItem.partida}`, 70, final, {
		  width: 30,
		  align: 'left'
		})
		doc.rect( 65, final - 3, 30, 40).stroke();

		doc.fontSize(10).text(`${requisicionItem.cantidad}`, 100, final, {
		  width: 40,
		  align: 'left'
		})
		doc.rect( 95, final - 3, 40, 40).stroke();

		doc.fontSize(10).text(`${requisicionItem.unidad}`, 140, final, {
		  width: 45,
		  align: 'left'
		})
		doc.rect( 135, final - 3, 45, 40).stroke();

		doc.fontSize(10).text(`${requisicionItem.descripcion}`, 185, final, {
		  width: 250,
		  align: 'justify'
		})
		doc.rect( 180, final - 3, 250, 40).stroke();

		doc.fontSize(10).text(`${requisicionItem.orden_compra}`, 435, final, {
		  width: 100,
		  align: 'justify'
		})
		doc.rect( 430, final - 3, 100, 40).stroke();

		final += 40
  }

  doc.fontSize(10).text(``, 65, final, {
	  width: 250,
	  align: 'justify'
	})

  doc.moveDown();
  doc.moveDown();


  doc.fontSize(14).text( `Tabla Comparativa` , { align: 'center' });
  doc.moveDown();

  final += 50


  doc.fontSize(8).text(`Partida`, 70, final ,{
	  width: 30,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Proveedor`, 100, final , {
	  width: 100,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Especificaciones:`, 200, final , {
	  width: 100,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Comentarios:`, 335, final , {
	  width: 100,
	  align: 'justify'
	})

	doc.fontSize(8).text(`Precio Cotizado`, 435, final , {
	  width: 180,
	  align: 'justify'
	})

	final += 15

  for (const requisicionEstimate of proveedores) {

  	doc.fontSize(10).text(`${requisicionEstimate.partida}`, 70, final, {
		  width: 30,
		  align: 'left'
		})
		doc.rect( 65, final - 3, 30, 40).stroke();

		doc.fontSize(10).text(`${requisicionEstimate.proveedor}`, 100, final, {
		  width: 100,
		  align: 'left'
		})
		doc.rect( 95, final - 3, 100, 40).stroke();

		doc.fontSize(10).text(`${requisicionEstimate.especificaciones}`, 200, final, {
		  width: 135,
		  align: 'left'
		})
		doc.rect( 195, final - 3, 135, 40).stroke();

		doc.fontSize(10).text(`${requisicionEstimate.comentarios}`, 335, final, {
		  width: 100,
		  align: 'justify'
		})
		doc.rect( 330, final - 3, 100, 40).stroke();

		doc.fontSize(10).text(`${requisicionEstimate.precio_cotizado}`, 435, final, {
		  width: 100,
		  align: 'justify'
		})
		doc.rect( 430, final - 3, 100, 40).stroke();

		final += 40
  }

  doc.fontSize(10).text(``, 65, final, {
	  width: 250,
	  align: 'justify'
	})

  doc.moveDown();

  doc.fontSize(12).text( `Seleccionado por: Comprador` , { align: 'left' });
  doc.moveDown();

  doc.fontSize(12).text( `Aprobado por: ${aprobo}` , { align: 'center' });
  doc.moveDown();

  doc.end();

}

module.exports = newPDF;
