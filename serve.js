// IMPORTAR DEPENDENCIAS
const express     = require("express");
const bodyParser  = require("body-parser");
var   cors        = require('cors');
const fileUpload  = require("express-fileupload");

// IMPORTAR EXPRESS
const app = express();

//Para servidor con ssl
var http = require('http');
const https = require('https');
const fs = require('fs');

// IMPORTAR PERMISOS
app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json({ limit: '1000000000mb'}));
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.raw({ type: 'audio/mpeg', limit: '600000mb' }));

app.use(fileUpload()); //subir archivos


app.use("/requisiciones.pdf",            express.static("../../requisiciones-pdf/"));
// Rutas estaticas
app.use('/fotos',                        express.static('../../fotos/'));
app.use('/pdfs',                         express.static('../../pdf/'));

app.use("/archivos.mikai",               express.static("../../archivos-mikai"));



// ----IMPORTAR RUTAS---------------------------------------->

require('./routes/rutasyusuarios/index.routes')(app);
require('./routes/cotizador/index.routes')(app);
require('./routes/corte/index.routes')(app);
require('./routes/formatos/formatos.routes')(app);
require('./routes/archivos/index.routes')(app);
require('./routes/requisiciones/index.routes')(app);


// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
// app.listen(3000, () => {
//   console.log("|************ Servidor Corriendo en el Puerto 3000 ************| ");
// });

//Para servidor con ssl

https.createServer({
    key: fs.readFileSync('/etc/letsencrypt/live/support-smipack.com/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/support-smipack.com/fullchain.pem'),
    passphrase: 'desarrollo'
}, app)
.listen(3011);
