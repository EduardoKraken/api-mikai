module.exports = app => {
  const users = require('../../controllers/cotizador/users.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/sessions"               , users.session);

  app.post("/cambia.estatus"         , users.cambiaEstatus);
  app.get("/users/:userId"           , users.findOne);
  
};