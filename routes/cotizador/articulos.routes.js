module.exports = app => {
  const articulos = require('../../controllers/cotizador/articulos.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/articulo.add",                  articulos.articuloAdd);
  app.get("/articulos.all",                  articulos.articulosAll);
  app.get("/articulo.id/:idarticulo",        articulos.getArticulo);
  app.put("/articulo.update/:idarticulo",    articulos.updateArticulo);
  app.delete("/articulo.delete/:idarticulo", articulos.deleteArticulo);

  app.post("/foto.delete/:idfoto",         articulos.deleteFoto);



  
};