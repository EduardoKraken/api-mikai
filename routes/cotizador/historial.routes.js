module.exports = app => {
  const historial = require('../../controllers/cotizador/historial.controllers') // --> ADDED THIS

  // Traer el historial de las cotizaciones
  app.post("/historial.add", historial.addHistorial);
  app.get("/historial.all",  historial.getHistorial);
  
};