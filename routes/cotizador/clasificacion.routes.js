module.exports = app => {
  const clasificacion = require('../../controllers/cotizador/clasificacion.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/clasificacion.add",                  clasificacion.clasificacionAdd);
  app.get("/clasificacion.all",                   clasificacion.clasificacionAll);
  app.get("/clasificacion.id/:idcatego",          clasificacion.getClasificacion);
  app.put("/clasificacion.update/:idcatego",      clasificacion.updateClasificacion);
  app.delete("/clasificacion.delete/:idcatego",   clasificacion.deleteClasificacion);

  // Clasificar el articulo
  app.post("/clasart.add",                     clasificacion.clasartAdd);
  app.delete("/clasart.delete/:idartcatego",   clasificacion.deleteClasart);


  app.post("/unidades.add",                     clasificacion.unidadesAdd);
  app.put("/unidades.update/:idunidades",       clasificacion.unidadesUpdate);
  app.get("/unidades.all",                      clasificacion.unidadesAll);

  
};