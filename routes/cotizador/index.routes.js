module.exports = app => {
  require("./articulos.routes")(app)
  require("./clasificacion.routes")(app)
	require("./correos.routes")(app)
	require("./descripcion.routes")(app)
	require("./documentos.routes")(app)
	require("./historial.routes")(app)
};