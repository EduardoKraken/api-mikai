module.exports = app => {
    const carpetas = require('../../controllers/archivos/carpetas.controllers') // --> ADDED THIS

    app.post("/carpetas.get.nivel", carpetas.getCarpetasNivel);
    app.get("/carpetas.principales/:id", carpetas.getCarpetasPrincipales);
    app.post("/carpetas.exacta", carpetas.getCarpetaExacta);
    app.post("/carpetas.agregar", carpetas.agregarCarpeta);
    app.post("/subir.archivos", carpetas.subirArchivo);

    app.get("/carpetas.disponibles", carpetas.carpetasDisponibles);
    app.post("/agregar.acceso", carpetas.agregarAcceso)
    app.delete("/eliminar.acceso/:id", carpetas.eliminarAcceso);

    app.post("/renombrar.carpeta", carpetas.renombrarCarpeta);
    app.delete("/eliminar.carpeta/:id", carpetas.eliminarCarpeta);
    app.delete("/eliminar.archivo/:id", carpetas.eliminarArchivo);

};