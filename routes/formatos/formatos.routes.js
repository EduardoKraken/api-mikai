module.exports = app => {
  const formatos = require('../../controllers/formatos/formatos.controllers') // --> ADDED THIS

  app.post("/formatos.add",                       formatos.addFormatos); 
  app.get("/formatos.id",                         formatos.getFormatosID);   
  app.get("/formatos.all",                        formatos.getAllFormatos);   
  app.put("/formatos.update/:id",                 formatos.updateFormatos);
  app.put("/formatos.update.consecutivo/:id",     formatos.updateFormatoConsecutivo);

  app.post("/agregar.nuevo.formulario",           formatos.agregar_nuevo_formulario); 

  
};