module.exports = app => {
  const descripcion = require('../../controllers/rutasyusuarios/descripcion.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/descripcion.add",                       descripcion.descripcionAdd);
  app.get("/descripcion.all",                        descripcion.descripcionAll);
  app.get("/descripcion.id/:iddescripcion",          descripcion.getDescripcion);
  app.put("/descripcion.update/:iddescripcion",      descripcion.updateDescripcion);
  app.delete("/descripcion.delete/:iddescripcion",   descripcion.deleteDescripcion);

  
};