module.exports = app => {
  require("./accesos.routes")(app)
  require("./rutas.routes")(app)
	require("./users.routes")(app)
	require("./descripcion.routes")(app)
};