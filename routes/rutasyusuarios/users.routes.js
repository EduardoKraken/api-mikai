module.exports = app => {
  const users = require('../../controllers/rutasyusuarios/users.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/sessions", users.session);
  app.get("/users/:userId", users.findOne);

  app.get("/users"                   , users.findAll);
  
  // RUTAS DE USUARIOS PARA LA PARTE DE USUARIOS DE LAS REQUISICIONES
  // app.get("/usuarios.requisiciones"              ,   users.usuariosRequisiciones);
  // app.put("/usuarios.requisiciones.update/:id"   ,   users.usuariosRequisicionesUpdate);
  // app.delete("/usuarios.requisiciones.delete/:id",   users.usuariosRequisicionesDelete);

  
  app.post("/user.add"               , users.create);
  app.post("/update.usuario"         , users.updateUsuario);
  app.put("/users.activar/:idusuario", users.activarUsuario);
  app.post("/olvida.contra"          , users.OlvideContra);
  app.post("/password.extra"         , users.passwordExtra);
  app.delete("/usuario.delete/:id"   , users.deleteUsuario);
  app.post("/pri/usuario/login"      , users.getUsauriosWataje);
  app.get("/users.accesos.carpetas"  , users.accesosCarpetas);
  
};


