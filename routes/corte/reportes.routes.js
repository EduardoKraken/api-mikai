module.exports = app => {
  const reportes = require('../../controllers/corte/reportes.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/cierre_detalle", reportes.cierreDetalle);
  app.post("/cierre_detalle_fecha", reportes.cierreDetalleFecha);
  // Usuarios que tienen acceso al corte de caja
  app.get("/user_caja", reportes.userCaja);
  app.post("/saldoinicial", reportes.saldoInicial);
  app.post("/retiros/:idcaja", reportes.retiros);
};