module.exports = app => {
  const users = require('../../controllers/corte/users.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/sessions", users.session);
  app.post("/registro", users.registro);

  app.post("/cambia.estatus", users.cambiaEstatus);
  app.post("/update.usuario", users.UpdateUsuario);
  app.post("/user.add", users.create);
  app.get("/users", users.findAll);
  app.get("/users/:userId", users.findOne);
  app.put("/users.activar/:idusuario", users.activarUsuario);
  app.post("/olvida.contra", users.OlvideContra);
  app.post("/password.extra", users.passwordExtra);
  app.put("/updateuser/:id", users.updatexId);

  app.delete("/usuario.delete/:id", users.deleteUsuario);

  

  // app.post("/user.email", users.getxEmail);


  // // Traer un usuario por email
  // // Update a Customer with customerId
  // app.put("/users/:userId", users.update);
  // // Activar usuario
  
};