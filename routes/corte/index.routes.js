module.exports = app => {
  require("./cajas.routes")(app)
  require("./movimientos.routes")(app)
		require("./reportes.routes")(app)
};