module.exports = app => {
  const cajas = require('../../controllers/corte/cajas.controllers') // --> ADDED THIS

  app.post("/abrir.caja"      , cajas.abrirCaja);
  app.post("/caja.abierta"    , cajas.cajaAbierta);
  app.post("/denominaciones"  , cajas.Denominaciones);
  app.post("/add.estatus.caja", cajas.addEstatusCaja);
  // Traer datos de la caja
  app.get("/caja.get/:idcaja" , cajas.getCaja);
  app.post("/cerrar.caja"     , cajas.cerrarCaja);
  app.post("/detalle.cierre"  , cajas.detCierre);

  app.post("/cajas.dia"       , cajas.getCajasDia);

  app.post("/cierre.delete"   , cajas.deleteCierre);


};