module.exports = app => {
  const movimiento = require('../../controllers/corte/movimientos.controllers') // --> ADDED THIS

  app.get("/movimientos/:id"  , movimiento.Movimientos);
  app.post("/add.movim"       , movimiento.addMovimiento);
  app.put("/movimiento/:id"   , movimiento.putMovimiento);
  app.put("/cancelacion/:id"  , movimiento.cancelacion);
  app.put("/devolucion/:id"   , movimiento.devolucion);
  app.post("/salida"          , movimiento.salida);

  app.post("/genera.corte"    , movimiento.generaCorte);
  app.get("/retiros/:id"      , movimiento.retiros);

  app.post("/movimientos.fecha"  , movimiento.MovimientosFecha);


  // app.post("/denominaciones", movimiento.Denominaciones);
  // app.post("/add.estatus.caja", movimiento.addEstatusCaja);
};