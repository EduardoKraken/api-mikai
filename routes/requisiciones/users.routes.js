module.exports = app => {
  const users = require('../../controllers/requisiciones/users.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  // RUTAS DE USUARIOS PARA LA PARTE DE USUARIOS DE LAS REQUISICIONES
  app.get("/usuarios.requisiciones"              ,   users.usuariosRequisiciones);
  app.put("/usuarios.requisiciones.update/:id"   ,   users.usuariosRequisicionesUpdate);
  app.delete("/usuarios.requisiciones.delete/:id",   users.usuariosRequisicionesDelete);

  
  app.post("/requi.registro"               , users.registro);
  app.post("/requi.user.add"               , users.create);
  app.post("/requi.update.usuario"         , users.UpdateUsuario);
  app.get("/requi.users"                   , users.findAll);
  app.put("/requi.updateuser/:id"          , users.updatexId);
  app.delete("/requi.usuario.delete/:id"   , users.deleteUsuario);
  app.post("/requi.pri/usuario/login"      , users.getUsauriosWataje);

};


