module.exports = app => {
  const departamentos = require('../../controllers/requisiciones/departamentos.controllers') // --> ADDED THIS

  app.get("/departamentos.list",                       departamentos.getDepartamentos);   
  app.post("/departamentos.add",                       departamentos.addDepartamento); 
  app.put("/departamentos.update/:id",                 departamentos.updateDepartamento);

};