module.exports = app => {
  const formatos = require('../../controllers/requisiciones/formatos.controllers') // --> ADDED THIS

  app.post("/formatos.add",                       formatos.addFormatos); 
  app.get("/formatos.id",                         formatos.getFormatosID);   
  app.get("/formatos.all",                        formatos.getAllFormatos);   
  app.put("/formatos.update/:id",                 formatos.updateFormatos);
  app.put("/formatos.update.consecutivo/:id",     formatos.updateFormatoConsecutivo);

};