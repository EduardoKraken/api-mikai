module.exports = app => {
  require("./correos.routes")(app)
  require("./departamentos.routes")(app)
  require("./requisiciones.routes")(app)
	require("./users.routes")(app)
};