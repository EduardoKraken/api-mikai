module.exports = app => {
  const requisiciones = require('../../controllers/requisiciones/requisiciones.controllers') // --> ADDED THIS

  app.get("/requisiciones.list",              requisiciones.getRequisiciones);   
  app.get("/requisiciones.departamento/:id",  requisiciones.getRequisicionesDepartamento);   
  app.get("/requisiciones.usuario/:id",       requisiciones.getRequisicionesUsuario);   
  app.post("/requisiciones.add",              requisiciones.addRequisiciones); 
  app.put("/requisiciones.update/:id",        requisiciones.updateRequisciones);
  app.post("/requisiciones.generar",          requisiciones.generarPDF);
  app.get("/requisiciones.notas/:id",         requisiciones.getNotasRequi);
  app.post("/requisiciones.notas",            requisiciones.addNotasRequi);

};