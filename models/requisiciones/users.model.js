const sql = require("../db.js");

// constructor
const users = function(user) {
  this.nombre       = user.nombre;
  this.apellidos    = user.apellidos;
  this.empresa      = user.empresa;
  this.telempresa   = user.telempresa;
  this.telmovil     = user.telmovil;
  this.email        = user.email;
  this.modmaquina   = user.modmaquina;
  this.matserie     = user.matserie;
  this.password     = user.password;
  this.estatus      = user.estatus;
};

// VARIABLES PARA QUERYS

users.session = (loger,result) =>{
  sql.query("SELECT * FROM usuarios WHERE email = ? AND password = ?"
    ,[loger.email, loger.password],(err, res) => {
    if (err) { 
      console.log("error: ", err); 
      result(err, null);
      return 
    }
    console.log('res',res)
    result(null, res[0])
  });
};

users.findOne = (userid, result) => {
  sql.query(`SELECT * FROM usuarios WHERE id = ${userid}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

users.usuariosRequisiciones = (  ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT a.*, u.nomuser, u.admin, r.ruta, d.iddepartamentos, d.departamento FROM accesos a
      LEFT JOIN usuarios u ON u.id = a.idusuario
      LEFT JOIN rutas r ON r.idruta = a.idruta
      LEFT JOIN departamentos d ON d.iddepartamentos = u.iddepartamentos
      WHERE r.idruta = 7;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

users.usuariosDepartamentos = (  ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT u.idusuarios, u.iddepartamentos, d.departamento, d.deleted, d.fecha_creacion FROM usuarios_departamentos u
        LEFT JOIN departamentos d ON d.iddepartamentos = u.iddepartamentos;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}


// Actualizar el ciclo en general
users.usuariosRequisicionesUpdate  = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE usuarios SET iddepartamentos = ?  WHERE id = ?`, [ a.iddepartamentos, id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id, a })
    })
  })
}

users.usuariosRequisicionesDelete  = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`DELETE FROM accesos WHERE idaccesos = ?;`, [ id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id })
    })
  })
}

users.deleteDepartamentosUsuario  = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`DELETE FROM usuarios_departamentos WHERE idusuarios_departamentos > 0 AND idusuarios = ?;`, [ id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id })
    })
  })
}


users.addDepartamentoUsuario = ( idusuarios, iddepartamentos ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO usuarios_departamentos( idusuarios, iddepartamentos ) VALUES ( ?, ? );`,
      [ idusuarios, iddepartamentos ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id: res.insertId })
    })
  })
}
module.exports = users;