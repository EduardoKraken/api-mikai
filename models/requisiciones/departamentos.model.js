const { result }  = require("lodash");
// Original
const sql      = require("../db.js");

//const constructor
const departamentos = function(departamentos) {};

// Consultar todos los departamentos
departamentos.getDepartamentos = (  ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM departamentos WHERE deleted = 0;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

// Agregar un ciclo+
departamentos.addDepartamento = ( pu ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO departamentos( departamento ) VALUES ( ? );`, [ pu.departamento ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id: res.insertId, ...pu })
    })
  })
}

// Actualizar el ciclo en general
departamentos.updateDepartamento = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE departamentos SET departamento = ?, deleted = ? WHERE iddepartamentos = ?`, [ a.departamento, a.deleted, id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id, a })
    })
  })
}


module.exports = departamentos;