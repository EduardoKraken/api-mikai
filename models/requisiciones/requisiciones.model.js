const { result }  = require("lodash");
// Original
const sql      = require("../db.js");

//const constructor
const requisiciones = function(requisiciones) {};

// Consultar todos los requisiciones
requisiciones.getRequisiciones = (  ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT r.*, u.nomuser AS solicito, u2.nomuser AS usuario, d.departamento, u3.nomuser AS aprobo FROM requisiciones r
			LEFT JOIN usuarios u ON u.id = r.idsolicito
			LEFT JOIN usuarios u2 ON u2.id = r.idusuario
			LEFT JOIN usuarios u3 ON u3.id = r.aprobada_usuario
			LEFT JOIN departamentos d ON d.iddepartamentos = r.iddepartamentos
			WHERE r.deleted = 0 ORDER BY r.fecha_creacion DESC;`,(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

requisiciones.getRequisicionesDepartamento = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT r.*, u.nomuser AS solicito, u2.nomuser AS usuario, d.departamento FROM requisiciones r
      LEFT JOIN usuarios u ON u.id = r.idsolicito
      LEFT JOIN usuarios u2 ON u2.id = r.idusuario
      LEFT JOIN departamentos d ON d.iddepartamentos = r.iddepartamentos
      WHERE r.deleted = 0 AND r.iddepartamentos IN (SELECT iddepartamentos FROM usuarios_departamentos WHERE idusuarios = ?)
      ORDER BY r.fecha_creacion DESC;`,[ id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

requisiciones.getRequisicionesUsuario = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT r.*, u.nomuser AS solicito, u2.nomuser AS usuario, d.departamento FROM requisiciones r
			LEFT JOIN usuarios u ON u.id = r.idsolicito
			LEFT JOIN usuarios u2 ON u2.id = r.idusuario
			LEFT JOIN departamentos d ON d.iddepartamentos = r.iddepartamentos
			WHERE r.deleted = 0 AND r.idsolicito = ? OR r.deleted = 0 AND r.idusuario = ?
      ORDER BY r.fecha_creacion DESC;`,[ id, id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}


requisiciones.articulosRequi = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM articulos_requi WHERE idrequisiciones IN (?);`,[ id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}

requisiciones.proveedoresRequi = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM proveedores_requi WHERE idrequisiciones IN (?);`,[ id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}


// Agregar un ciclo+
requisiciones.addRequisiciones = ( r ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO requisiciones( idsolicito, idusuario, folio, estatus, iddepartamentos ) VALUES ( ?, ?, ?, ?, ? );`,
    	[ r.idsolicito, r.idusuario, r.folio, r.estatus, r.iddepartamentos ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id: res.insertId, ...r })
    })
  })
}

requisiciones.addArticulosRequi = ( r, id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO articulos_requi( partida, cantidad, unidad, descripcion, orden_compra, idrequisiciones ) VALUES ( ?, ?, ?, ?, ?, ? );`,
    	[ r.partida, r.cantidad, r.unidad, r.descripcion, r.orden_compra, id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id: res.insertId, ...r })
    })
  })
}

requisiciones.addProveedoresRequi = ( r, id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO proveedores_requi( proveedor, partida, especificaciones, comentarios, precio_cotizado, idrequisiciones ) VALUES ( ?, ?, ?, ?, ?, ? );`,
    	[ r.proveedor, r.partida, r.especificaciones, r.comentarios, r.precio_cotizado, id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id: res.insertId, ...r })
    })
  })
}

requisiciones.addNotasRequi = ( idsolicito, notas, id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO notas_requi( idusuario, notas, idrequisiciones ) VALUES ( ?, ?, ? );`,
      [ idsolicito, notas, id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id: res.insertId  })
    })
  })
}

// Actualizar el ciclo en general
requisiciones.updateRequisciones = ( id, a ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`UPDATE requisiciones SET estatus = ?, deleted = ? 
    	${ a.aprobada_usuario ? ', aprobada_usuario = ' + a.aprobada_usuario : ' ' }
    	${ a.aprobada_usuario ? ', aprobada_fecha   = NOW()' : ' '}
    	WHERE idrequisiciones = ?;`, [ a.estatus, a.deleted, id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve({ id, a })
    })
  })
}


requisiciones.getNotasRequi = ( id ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT n.*, u.nomuser FROM notas_requi n
      LEFT JOIN usuarios u ON u.id = n.idusuario
      WHERE n.idrequisiciones = ?;`,[ id ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }) }
      resolve(res)
    })
  })
}


module.exports = requisiciones;