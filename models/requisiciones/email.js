var nodemailer = require('nodemailer');
const fs = require('fs');

exports.enviarRequisicion = (req, res) => {
  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    requireTLS: true,
    auth: {
        user: 'sofsolution.dev@gmail.com',
        pass: 'desarrollo'
    }
  });

  var mailOptions = {
    from: '"MIKAI" <info@testsnode.com>',
    to:  ` "${req.body.correo}"`,
    subject: `Requisiciones RQ-${ req.body.idrequisiciones }`,
    text: 'Hola, anexo requisición',
    html: ``,
    attachments: [
    	{
    		filename: `RQ-${ req.body.idrequisiciones }.pdf`,
      	path    : `https://support-smipack.com/requisiciones-pdf/RQ-${ req.body.idrequisiciones }.pdf`
    	}
    ]
  };

  transporter.sendMail(mailOptions, (error, info) =>{
    if (error) {
    	console.log( error )
      res.status( 400 ).send({ message: 'Error al enviar el correo' })
    } else {
      console.log('Email sent: ' + info.response);
      res.send({ message: '' })
    }
  });
}