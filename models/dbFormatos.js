const mysql    = require('mysql');
const dbConfig = require("../config/dbFormatos.js");

// Create a connection to the database
const connection = mysql.createConnection({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB,
  dateStrings:true
});

// open the MySQL connection
connection.connect(error => {
  if (error) throw error;
  console.log("|********* CONECCION A" +" "+ dbConfig.DB  +" "+ "CORRECTAMENTE **********|");


});

module.exports = connection;