const sql = require("../db.js");

const Cajas = function(cajas) {
  this.id         = cajas.id;
};

Cajas.cajaAbierta = (idusuario, result) => {
  sql.query(`SELECT idcaja FROM estatuscaja WHERE idusuario = ?`,[idusuario.idusuario] ,(err, res) => {
    if (err) { result(null, err); return; }
    console.log("cajas activa: ", res);
    result(null, res);
  });
};

Cajas.abrirCaja = (newCaja, result) => {
  sql.query("INSERT INTO caja SET ?", newCaja, (err, res) => {
    if (err) { result(err, null); return; }
    console.log("abriendo caja: ", { id: res.insertId, ...newCaja });
    result(null, { id: res.insertId, ...newCaja });
  });
};

Cajas.Denominaciones = (denominacion, result) => {
  sql.query("INSERT INTO denominaciones SET ?", denominacion, (err, res) => {
    if (err) { result(err, null); return; }
    console.log("creando denominaciones: ", { id: res.insertId, ...denominacion });
    result(null, { id: res.insertId, ...denominacion });
  });
};

Cajas.addEstatusCaja = (estatuscaja, result) => {
  sql.query("INSERT INTO estatuscaja SET ?", estatuscaja, (err, res) => {
    if (err) { result(err, null); return; }
    console.log("agregando caja activa: ", { id: res.insertId, ...estatuscaja });
    result(null, { id: res.insertId, ...estatuscaja });
  });
};

Cajas.getCaja = (idcaja, result) => {
  sql.query("SELECT * FROM caja WHERE idcaja = ?", idcaja, (err, res) => {
    if (err) { result(null, err); return; }
    console.log("cajas activa: ", res);
    result(null, res[0]);
  });
};

Cajas.cerrarCaja = (outCaja, result) => {
  sql.query("INSERT INTO cierre SET ?", outCaja, (err, res) => {
    if (err) { result(err, null); return; }

    sql.query("DELETE FROM estatuscaja WHERE idcaja = ? AND idestatuscaja > 0", outCaja.id_caja, (err, res2) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res2.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("cerrando caja: ", { id: res.insertId, ...outCaja });
      result(null, { id: res.insertId, ...outCaja });
    });

  });
};

Cajas.detCierre = (detOutCaja, result) => {
  sql.query("INSERT INTO det_cierre SET ?", detOutCaja, (err, res) => {
    if (err) { result(err, null); return; }
    console.log("Agregando detalle de cierre de caja: ", { id: res.insertId, ...detOutCaja });
    result(null, { id: res.insertId, ...detOutCaja });
  });
};


// Cajas.getCajasDia = (fecha, result) => {
//   sql.query(`SELECT c.idcaja, c.fecha, c.hora, c.idusuario, c.saldoinicial, u.nomuser, e.idestatuscaja FROM caja c 
//   LEFT JOIN usuarios u ON u.id = c.idusuario 
//   LEFT JOIN estatuscaja e ON e.idusuario = u.id
//   WHERE c.fecha = ?;`, fecha, (err, res) => {
//   if (err) { result(null, err); return; }
//     console.log("cajas activa: ", res);
//     result(null, res);
//   });
// };

Cajas.getCajasDia = (fecha, result) => {
  sql.query(`SELECT c.idcaja, c.fecha, c.hora, c.idusuario, c.saldoinicial, u.nomuser, e.idestatuscaja FROM caja c 
  LEFT JOIN usuarios u ON u.id = c.idusuario 
  LEFT JOIN estatuscaja e ON e.idusuario = u.id
  WHERE c.fecha = ?;`, fecha, (err, res) => {
    if (err) { result(null, err); return; }
    sql.query(`SELECT SUM(cantidad) AS pago, idcaja FROM movimientos WHERE tipo = 2 AND tipopago = 1 GROUP BY idcaja;`, (err, resta) => {
    if (err) { result(null, err); return; }
      if (err) { result(null, err); return; }
        sql.query(`SELECT SUM(cantidad) AS pago, idcaja FROM movimientos WHERE tipo = 1 AND tipopago = 1 GROUP BY idcaja;`, (err, suma) => {
          if (err) { result(null, err); return; }
          console.log('resta',resta)
          console.log('suma',suma)
          var resultado = []
          for(const i in res){
            resultado.push(res[i])
          }

          for(const i in resultado){
            for(const j in resta){
              if(resultado[i].idcaja == resta[j].idcaja){
                console.log("si")
                resultado[i].saldoinicial = resultado[i].saldoinicial - resta[j].pago
              }
            }
          }

          for(const i in resultado){
            for(const j in suma){
              if(resultado[i].idcaja == suma[j].idcaja){
                console.log("si")
                resultado[i].saldoinicial = resultado[i].saldoinicial + suma[j].pago
              }
            }
          }
            console.log("cajas activa: ", resultado);
            result(null, resultado);
          });
      });
    });
};

Cajas.deleteCierre = (datos, result) => {
  sql.query(`SELECT * FROM cierre WHERE id_caja = ? AND id_usuario = ?;`, [datos.idcaja, datos.idusuario], (err, idcierre) => {
    console.log("idcierre", idcierre)
    if (err) { result(null, err); return; }
    sql.query("DELETE FROM cierre WHERE id_caja = ? AND id_usuario = ? AND id > 0;", [datos.idcaja, datos.idusuario], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }

      sql.query("DELETE FROM det_cierre WHERE id_cierre = ? AND id > 0;", idcierre[0].id, (err, res2) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }

        if (res2.affectedRows == 0) {
          // not found Customer with the id
          result({ kind: "not_found" }, null);
          return;
        }

         result(null, res2);
      });
    });
  });
};

/* Obtener los datos de la caja*/

module.exports = Cajas;