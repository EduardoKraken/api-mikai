const sql = require("../db.js");

// constructor
const Reportes = (r)=> {
  this.nomuser = user.nomuser;
  this.apellidos = user.apellidos;
  this.empresa = user.empresa;
  this.telempresa     = user.telempresa;
  this.telmovil   = user.telmovil;
  this.email   = user.email;
  this.modmaquina    = user.modmaquina;
  this.matserie  = user.matserie;
  this.password     = user.password;
  this.estatus   = user.estatus;
};

// VARIABLES PARA QUERYS

Reportes.cierreDetalle = (data,result) =>{
  sql.query(`SELECT c.id as "idcierre", c.id_caja, c.total, d.id AS "iddetalle", d.tipo, d.cantidad AS "real", d.reportado, (d.cantidad - d.reportado) AS "diferencia" 
    FROM cierre c INNER JOIN det_cierre d ON c.id = d.id_cierre
      WHERE c.id_usuario = ? AND c.fecha = ?;`,[data.id, data.fecha],(err, res) => {
    if (err) { 
      console.log("error: ", err); 
      result(err, null);
      return 
    }
    console.log('res',res)
    result(null, res)
  });
};

Reportes.cierreDetalleFecha = (data,result) =>{
  sql.query(`SELECT c.id as "idcierre", c.id_caja, c.total, d.id AS "iddetalle", d.tipo, d.cantidad AS "real", d.reportado, (d.cantidad - d.reportado) AS "diferencia" 
FROM cierre c INNER JOIN det_cierre d ON c.id = d.id_cierre
WHERE c.fecha = ?;`,[data.fecha],(err, res) => {
    if (err) { 
      console.log("error: ", err); 
      result(err, null);
      return 
    }
    console.log('res',res)
    result(null, res)
  });
};

Reportes.userCaja = (result) => {
  sql.query(`SELECT u.id, u.nomuser FROM usuarios u INNER JOIN accesos a ON u.id = a.idusuario WHERE a.idruta = 4;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res);
      result(null, res);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

Reportes.saldoInicial = (fecha,result) => {
  sql.query(`SELECT SUM(saldoinicial) AS saldoinicial FROM caja WHERE fecha = ?;`,fecha,(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

Reportes.retiros = (idcaja,result) => {
  sql.query(`SELECT * FROM retiros WHERE idcaja = ?;`,idcaja,(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res);
      result(null, res);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};


module.exports = Reportes;