const sql = require("../db.js");

const Movimientos = function(movimientos) {
  this.id         = movimientos.id;
};

Movimientos.Movimientos = (idcaja, result) => {
  sql.query(`SELECT m.idmovimientos, m.cantidad, m.concepto, m.folio, m.tipopago, m.tipo, m.cambio, m.pago, m.autorizacion, m.concepto2, m.hora, m.idcaja, u.nomuser 
FROM movimientos m LEFT JOIN usuarios u ON u.id = m.autorizacion
WHERE idcaja = ?;`,[idcaja] ,(err, res) => {
    if (err) { result(null, err); return; }
    console.log("Movimientos activa: ", res);
    result(null, res);
  });
};

Movimientos.MovimientosFecha = (fecha, result) => {
  sql.query(`SELECT m.idmovimientos, m.cantidad, m.concepto, m.folio, m.tipopago, m.tipo, m.cambio, m.pago, m.autorizacion, m.concepto2, m.hora, m.idcaja, u.nomuser, c.fecha 
FROM movimientos m LEFT JOIN usuarios u ON u.id = m.autorizacion LEFT JOIN caja c ON c.idcaja = m.idcaja
WHERE c.fecha =  ?;`,[fecha] ,(err, res) => {
    if (err) { result(null, err); return; }
    console.log("Movimientos activa: ", res);
    result(null, res);
  });
};

Movimientos.addMovimiento = (movim, result) => {
  sql.query("INSERT INTO movimientos SET ?", movim, (err, res) => {
    if (err) { result(err, null); return; }
    console.log("insertando movimientos: ", { id: res.insertId, ...movim });
    result(null, { id: res.insertId, ...movim });
  });
};

Movimientos.putMovimiento = (id, movim, result) => {
  sql.query(`UPDATE movimientos SET cantidad=?, concepto=?, folio=?, tipopago=?, tipo=?, cambio=?, pago=?, concepto2=? WHERE idmovimientos=?`, 
              [movim.cantidad,movim.concepto,movim.folio,movim.tipopago, movim.tipo, movim.cambio, movim.pago,movim.concepto2, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      console.log("actualizando movimiento: ", { id: id, ...movim });
      result(null, { id: id, ...movim });
    }
  );
};

Movimientos.cancelacion = (id, movim, result) => {
  sql.query(`UPDATE movimientos SET tipo=?, concepto2=?, autorizacion=? WHERE idmovimientos=?`, 
              [movim.tipo, movim.concepto2, movim.autorizacion, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      console.log("cancelando movimiento: ", { id: id, ...movim });
      result(null, { id: id, ...movim });
    }
  );
};

Movimientos.devolucion = (id, movim, result) => {
  sql.query(`UPDATE movimientos SET tipo=?, concepto2=?, autorizacion=? WHERE idmovimientos=?`, 
              [movim.tipo, movim.concepto2, movim.autorizacion, id],
    (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      console.log("devolución de movimiento: ", { id: id, ...movim });
      result(null, { id: id, ...movim });
    }
  );
};

Movimientos.salida = (movim, result) => {
  sql.query("INSERT INTO movimientos SET ?", movim, (err, res) => {
    if (err) { result(err, null); return; }
    console.log("SALIDA DE MOVIMIENTOS: ", { id: res.insertId, ...movim });
    result(null, { id: res.insertId, ...movim });
  });
};

Movimientos.generaCorte = (corte, result) => {
  sql.query("INSERT INTO retiros SET ?", corte, (err, res) => {
    if (err) { result(err, null); return; }
    console.log("insertando corte: ", { id: res.insertId, ...corte });
    result(null, { id: res.insertId, ...corte });
  });
};

Movimientos.retiros = (idcaja, result) => {
  sql.query(`SELECT * FROM retiros WHERE idcaja = ?`,[idcaja] ,(err, res) => {
    if (err) { result(null, err); return; }
    console.log("Consultando retiros: ", res);
    result(null, res);
  });
};

module.exports = Movimientos;