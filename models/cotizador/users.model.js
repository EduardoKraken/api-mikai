const sql = require("../db.js");

// constructor
const Users = function(user) {
  this.nomuser = user.nomuser;
  this.apellidos = user.apellidos;
  this.empresa = user.empresa;
  this.telempresa     = user.telempresa;
  this.telmovil   = user.telmovil;
  this.email   = user.email;
  this.modmaquina    = user.modmaquina;
  this.matserie  = user.matserie;
  this.password     = user.password;
  this.estatus   = user.estatus;
};

// VARIABLES PARA QUERYS

Users.login = (loger,result) =>{
  sql.query("SELECT * FROM usuarios WHERE email = ? AND password = ?"
    ,[loger.email, loger.password],(err, res) => {
    if (err) { 
      console.log("error: ", err); 
      result(err, null);
      return 
    }
    console.log('res',res)
    result(null, res)
  });
};

Users.validaEmail = (usuario, result) =>{
  sql.query("SELECT email FROM usuarios WHERE email = ?",[usuario.email],(err, res) => {
    if (err) { result(err, null);  return  }
    result(null, res)
  });
};


Users.cambiaEstatus = (user, result)=>{
  sql.query("UPDATE usuarios SET sesion=? WHERE id = ?", [user.sesion, user.id], (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      result(null, res);
    }
  );
};

Users.findById = (userid, result) => {
  console.log("Recibo", userid)
  sql.query(`SELECT * FROM usuarios WHERE id = ${userid}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

module.exports = Users;