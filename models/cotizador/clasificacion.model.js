const sql = require("../db.js");

// constructor
const Clasificacion = (catego)=> {
  this.idcatego      = catego.idcatego;
  this.nomcatego     = catego.nomcatego;
};

// VARIABLES PARA QUERYS
// Agregar un artículo
Clasificacion.clasificacionAdd = (c, result) => {
  sql.query(`INSERT INTO catego(nomcatego)VALUES(?)`,[c.nomcatego], (err, res) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
  });
};

// Traer todos los artículos
Clasificacion.clasificacionAll = (result) => {
  sql.query(`SELECT * FROM catego ORDER BY nomcatego;`, (err, catego) => { 
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    var res = []
    sql.query(`SELECT d.iddescripcion, d.descripcion, d.idcatego, d.precio, u.idunidades, u.unidad, d.factor FROM descripcion d LEFT JOIN unidades u ON u.idunidades = d.idunidades;`, (err, descripcion) => { 
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      for(const i in catego){
        res.push({
          idcatego: catego[i].idcatego,
          nomcatego: catego[i].nomcatego,
          descripcion: [],
        })
        for(const j in descripcion){
          if(catego[i].idcatego == descripcion[j].idcatego){
            res[i].descripcion.push(descripcion[j])
          }
        }
      }
      console.log("Artículos: ", res);
      result(null, res);
    });   
  });
};


Clasificacion.getClasificacion = (idcatego, result) => {
  sql.query(`SELECT * FROM catego WHERE idcatego = ${idcatego}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};


Clasificacion.updateClasificacion = (id, art, result)=>{
  sql.query(` UPDATE catego SET nomcatego = ?
                WHERE idcatego = ?`, [ art.nomcatego, id], 
  (err, res) =>  {

      if (err) { 
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }
      // console.log("Se actualizo el user: ", { id: id, ...user });
      result(null, "Usuario actualizado correctamente");
    }
  );
};


Clasificacion.deleteClasificacion = (id, result) => {
  sql.query("DELETE FROM catego WHERE idcatego = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

// **********************************************************************
// **********************************************************************
// **********************************************************************
// **********************************************************************

Clasificacion.clasartAdd = (c, result) => {
  sql.query(`INSERT INTO artcatego(idarticulo, idcatego)VALUES(?,?)`,[c.idarticulo,c.idcatego], (err, res) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
  });
};

Clasificacion.deleteClasart = (id, result) => {
  sql.query("DELETE FROM artcatego WHERE idartcatego = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};



/**********************************/
/**********************************/
/**********************************/

Clasificacion.unidadesAdd = (c, result) => {
  sql.query(`INSERT INTO unidades(unidad,tipo,lados,factor1,factor2,factor3)VALUES(?,?,?,?,?,?)`,[c.unidad,c.tipo,c.lados,c.factor1,c.factor2,c.factor3], 
    (err, res) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Unidad: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
  });
};

Clasificacion.unidadesAll = (result) => {
  sql.query(`SELECT * FROM unidades`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res);
      result(null, res);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};


Clasificacion.unidadesUpdate = (id, un, result)=>{
  sql.query(` UPDATE unidades SET unidad = ?, tipo = ?, lados = ?, factor1 = ?, factor2 = ?, factor3 = ?
                WHERE idunidades = ?`, [ un.unidad,un.tipo,un.lados,un.factor1,un.factor2,un.factor3, id], 
  (err, res) =>  {

      if (err) { 
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }
      // console.log("Se actualizo el user: ", { id: id, ...user });
      result(null, "Usuario actualizado correctamente");
    }
  );
};


module.exports = Clasificacion;


