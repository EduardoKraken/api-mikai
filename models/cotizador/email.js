// const sgMail = require('@sendgrid/mail');
// sgMail.setApiKey('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') // Input Api key or add to environment config
// sgMail.setSubstitutionWrappers('%', '%');

var nodemailer = require('nodemailer');
const pdf = require('html-pdf');
const fs = require('fs');

exports.senEmail = (req, res) => {

  const content = `<!DOCTYPE html>
      <html lang="en">
      <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      </head>
      <body>

      <div class="container" id="container">
        <img src="https://www.mikai.mx/wp-content/uploads/2019/11/mikai-logo.png" height="80" ></img>  
        <h4>No. Cotización: ${req.body.no} </h4>        
        <hr/>
        <div style="text-align: center; " ><h3><b>${req.body.nombre}</b></h3></div>
        <div style="text-align: center; " >
          <img src="${req.body.foto}" height="180" ></img>
        </div>

        <div style="text-align: left; " >
         NOTA: ${req.body.nota}
        </div>
                  
        <br/>
        <br/>
        <br/>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Descripción</th>
              <th>Medida</th>
              <th>Unidad</th>
            </tr>
          </thead>
          <tbody>
          <br/>
            ${req.body.html}
          </tbody>
        </table>
          <div style="text-align: right;"><h4><b>Total: ${req.body.total}</b></h4></div>
      </div>
      <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
      <script>
          new Vue({
            el:'#container',
            data(){
              return{
                nombre:'eduardo daniel '
              }
            }
          })
        </script>
      </body>
      </html>

    `

  const content2 = `<!DOCTYPE html>
    <html lang="en">
    <head>
      <title>Bootstrap Example</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container" id="container">
      <img src="https://www.mikai.mx/wp-content/uploads/2019/11/mikai-logo.png" height="80" ></img>  
      <h4>No. Cotización: ${req.body.no} </h4>        
      <hr/>
      <div style="text-align: center; " ><h3><b>${req.body.nombre}</b></h3></div>
      <div style="text-align: center; " >
        <img src="${req.body.foto}" height="180" ></img>
      </div>
      <br/>
      <div style="text-align: left; " >
      NOTA: ${req.body.nota}
      </div>
                
      <br/>
      <br/>
      <br/>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Descripción</th>
            <th>Cantidad</th>
            <th>Unidad</th>
          </tr>
        </thead>
        <tbody>
          ${req.body.html}
        </tbody>
      </table>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script>
        new Vue({
          el:'#container',
          data(){
            return{
              nombre:'eduardo daniel '
            }
          }
        })
      </script>
    </body>
    </html>
  `
  console.log('nombre de la cotización',req.body.no)
  pdf.create(content).toFile('./../../pdf/' + req.body.no + '.pdf' , (err, respuesta)=> {
      if (err){
        console.log(err);
      } else {
        fs.readFile('./../../pdf/' + req.body.no + '.pdf',  (err, data)=> {

          pdf.create(content2).toFile('./../../pdf/' + req.body.no + '2.pdf', (err, respuesta)=> {
            if (err){
              console.log(err);
            } else {
              fs.readFile('./../../pdf/' + req.body.no + '2.pdf',  (err, data2)=> {

                // let transporter = nodemailer.createTransport({
                //   host: 'smtp.gmail.com',
                //   port: 587,
                //   requireTLS: true,
                //   auth: {
                //       user: 'sofsolution.dev@gmail.com',
                //       pass: 'desarrollo'
                //   }
                // });

                const user = 'sofsolution.dev@gmail.com'
                const pass = 'ldeoykcqgsbhvxzc'

                // Preparamos el transporte
                let transporter = nodemailer.createTransport({
                  host: 'smtp.gmail.com',
                  port: 465, 
                  secure:true,
                  auth: {
                    user,
                    pass
                  },
                  from: user
                });

                var mailOptions = {
                  from: '"MIKAI" <info@testsnode.com>',
                  to:  ` "${req.body.correo}"`,
                  subject: 'Cotización',
                  text: 'Hola, anexo cotización solicitada',
                  // attachments: [{'filename': 'cotizacion.pdf', 'content': data},{'filename': 'cotizacion-2.pdf', 'content': data2}]
                  attachments: [
                    {
                      filename: `cotizacion.pdf`,
                      content : data
                    },
                    {
                      filename: `cotizacion-2.pdf`,
                      content : data2
                    }
                  ]

                };

                transporter.sendMail(mailOptions, (error, info) =>{
                  if (error) {
                    console.log(error);
                  } else {
                    console.log('Email sent: ' + info.response);
                    res.send(info.response)
                  }
                });


              });
            }
          }); 

        });
      }
    }); 
}

exports.sendContra = (req, res) => {
  var nodemailer = require('nodemailer');

  console.log(req.body.correo)

  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    requireTLS: true,
    auth: {
        user: 'sofsolution.dev@gmail.com',
        pass: 'desarrollo'
    }
  });


  var mailOptions = {
    from: '"SMIPACK" <info@testsnode.com>',
    to:  ` "${req.body.correo}"`,
    subject: 'Activar usuario',
    text: 'That was easy!',
    html: `<!DOCTYPE html><html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"><!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"><!--<![endif]-->

    <style type="text/css">
      body, p, div {
        font-family: arial;
        font-size: 14px;
      }
      body {
        color: #000000;
      }
      body a {
        color: #1188E6;
        text-decoration: none;
      }
      p { margin: 0; padding: 0; }
      table.wrapper {
        width:100% !important;
        table-layout: fixed;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: 100%;
        -moz-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
      }
      img.max-width {
        max-width: 100% !important;
      }
      .column.of-2 {
        width: 50%;
      }
      .column.of-3 {
        width: 33.333%;
      }
      .column.of-4 {
        width: 25%;
      }
      @media screen and (max-width:480px) {
        .preheader .rightColumnContent,
        .footer .rightColumnContent {
            text-align: left !important;
        }
        .preheader .rightColumnContent div,
        .preheader .rightColumnContent span,
        .footer .rightColumnContent div,
        .footer .rightColumnContent span {
          text-align: left !important;
        }
        .preheader .rightColumnContent,
        .preheader .leftColumnContent {
          font-size: 80% !important;
          padding: 5px 0;
        }
        table.wrapper-mobile {
          width: 100% !important;
          table-layout: fixed;
        }
        img.max-width {
          height: auto !important;
          max-width: 480px !important;
        }
        a.bulletproof-button {
          display: block !important;
          width: auto !important;
          font-size: 80%;
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
        .columns {
          width: 100% !important;
        }
        .column {
          display: block !important;
          width: 100% !important;
          padding-left: 0 !important;
          padding-right: 0 !important;
          margin-left: 0 !important;
          margin-right: 0 !important;
        }
        .total_spacer {
          padding:0px 0px 0px 0px;
        }
      }
    </style>
    <!--user entered Head Start-->
    
     <!--End Head user entered-->
  </head>
  <body>
    <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ebebeb;">
      <div class="webkit">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ebebeb">
          <tbody><tr>
            <td valign="top" bgcolor="#ebebeb" width="100%">
              <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                <tbody><tr>
                  <td width="100%">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tbody><tr>
                        <td>
                          <!--[if mso]>
                          <center>
                          <table><tr><td width="600">
                          <![endif]-->
                          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                            <tbody><tr>
                              <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                
    <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
      <tbody><tr>
        <td role="module-content">
          <p></p>
        </td>
      </tr>
    </tbody></table>
        <!--<div style="background-color: #757575; padding: 10px; text-align: center">-->
        <!--                              <span style="color: white ; font-weight: bold; font-size: 15px"></span>-->
        <!--                            </div>-->
    <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tbody><tr>
        <td style="font-size:6px;line-height:10px;padding:20px 0px 20px 0px;" valign="top" align="center">
          <img  border="0" style="display:block;color:#000000;height:100% !important;" src="https://www.support-smipack.com/fotos/logo.png" alt="" >
        </td>
      </tr>
    </tbody></table>

    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tbody><tr>
        <td style="line-height:22px;text-align:inherit;" height="100%" valign="top" bgcolor="">
          <div style="background-color:#005dac; padding: 10px; text-align: center">
                                      <div style="text-align: center;"><strong><span style="color:white;"><span style="font-size:12px;"> Hola, `+req.body.nomuser+`</span></span></strong></div>
                                    </div>
            
        </td>
      </tr>
    </tbody>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tbody><tr>
        <td style="font-weight:bold; font-size:12;padding:20px 20px 20px 35px;line-height:30px;text-align:inherit;" height="80%" valign="top" bgcolor="">
            <div style="text-align: center;">
                <span style="color:black;">
                    SE GENERO UNA SOLICITUD DE CAMBIO DE CONTRASEÑA
                </span>
            </div>
            
            <div style="text-align: center;">
                <span style="color:black;">
                    ESTÉ ES TU CÓDIGO DE ACCESO
                </span>
            </div>
            <br>
            <div style="text-align: center;  ">
                <span style="color:black; font-size:25px;">
                    `+req.body.codigo+`
                </span>
            </div>
            <br>
        </td>
      </tr>
    </tbody></table>
    
    
      <div style="background-color:#7cc0e9; padding: 10px; text-align: center">
        <span style="color: white ; font-weight: bold; font-size: 10px">
        Sistema creado por SofSolution-2020</span>
      </div>
    <table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tbody><tr>
        <td style="padding:0px 0px 0px 0px;" role="module-content" height="100%" valign="top" bgcolor="">
          <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="5px" style="line-height:5px; font-size:5px;">
            <tbody><tr>
              <td style="padding: 0px 0px 5px 0px;" bgcolor="#ebebeb"></td>
            </tr>
          </tbody></table>
        </td>
      </tr>
    </tbody></table>


                              <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                <tbody><tr>
                                  <td style="padding:5px 20px 20px 20px;line-height:0px;text-align:inherit;background-color:#F5F5F5;" height="100%" valign="top" bgcolor="#F5F5F5">
                                      <div style="text-align: center;"><span style="color:#7a7a7a;"><span style="font-size:11px;">Sistema de Control de Acceso. Inicia sesión con el siguiente código de seguridad.</span></span></div>
                                  </td>
                                </tr>
                              </tbody></table>
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>
        </tbody></table>
      </div>
    </center>
  

    </body></html>
  `
  };

  transporter.sendMail(mailOptions, (error, info) =>{
    if (error) {
      console.log(error);
    } else {
      res.send(info.response)
      console.log('Email sent: ' + info.response);
    }
  });
}
exports.sendNodemailer = (req, res) => {
}