const sql = require("../db.js");

// constructor
const Historial = function(user) {

};

// VARIABLES PARA QUERYS

Historial.addHistorial = (c,result) =>{
  sql.query(`INSERT INTO historialcoti(idusuariosweb,fecha,hora,nota,nomdocum)VALUES(?,?,?,?,?)`,
  	[c.idusuariosweb,c.fecha,c.hora,c.nota,c.nomdocum], (err, res) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
  });
};


Historial.getHistorial = (result) => {
  sql.query(`SELECT h.idhistorialcoti, h.idusuariosweb, h.fecha, h.hora, h.nota, h.nomdocum, u.nomuser FROM historialcoti h INNER JOIN usuarios u ON u.id = h.idusuariosweb;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("Historial: ", res);
    result(null, res);
  });
};

module.exports = Historial;