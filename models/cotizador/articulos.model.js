const sql = require("../db.js");

// constructor
const Articulos = function(art) {
  this.idarticulo = art.idarticulo;
  this.nomart     = art.nomart;
  this.cant       = art.cant;
  this.precio     = art.precio;
};

// VARIABLES PARA QUERYS
// Agregar un artículo
Articulos.articuloAdd = (c, result) => {
  sql.query(`INSERT INTO articulos(nomart,cant,precio,notas)VALUES(?,?,?,?)`,
    [c.nomart,c.cant,c.precio,c.notas], (err, resart) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    for(const i in c.imagename){
      sql.query(`INSERT INTO fotos(imagename,idarticulo)VALUES(?,?)`,
        [c.imagename[i].imagename, resart.insertId], (errfoto, resfoto) => { 
        if (errfoto) {
          console.log("error: ", errfoto);
          result(errfoto, null);
          return;
        }
      });
    }
    console.log("Crear Grupo: ", { id: resart.insertId, ...c });
    result(null, { id: resart.insertId, ...c });
  });
};

// Traer todos los artículos
Articulos.articulosAll = (result) => {
  sql.query(`SELECT * FROM articulos ORDER BY nomart;`, 
    (err, articulos) => { 
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }else{
      var res = []
      sql.query(`SELECT a.idarticulo, IFNULL(ac.idcatego,'') AS idcatego, IFNULL(c.nomcatego,'') AS nomcatego, ac.idartcatego, a.notas  
        FROM articulos a INNER JOIN artcatego ac ON a.idarticulo = ac.idarticulo LEFT JOIN catego c ON c.idcatego = ac.idcatego;`, 
        (err, categorias) => { 
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
        sql.query(`SELECT d.iddescripcion, d.descripcion, d.idcatego, d.precio, c.idcatego, c.nomcatego, u.idunidades, u.unidad, d.factor, u.tipo, u.lados, u.factor1, u.factor2, u.factor3 
          FROM descripcion d LEFT JOIN catego c ON d.idcatego = c.idcatego LEFT JOIN unidades u ON u.idunidades = d.idunidades;`, 
          (err, descripcion) => { 
          if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }
          sql.query(`SELECT f.idfoto, a.idarticulo, a.nomart, a.precio, a.cant, a.notas, ifnull(f.imagename, '') AS 'imagename'
            FROM articulos a 
            LEFT JOIN  fotos f ON a.idarticulo = f.idarticulo;`, 
          (err, fotos) => { 
            if (err) {
              console.log("error: ", err);
              result(null, err);
              return;
            }
            for(const i in articulos){
              res.push({
                idarticulo: articulos[i].idarticulo,
                notas: articulos[i].notas,
                imagename: [],
                nomart: articulos[i].nomart,
                precio: articulos[i].precio,
                categorias: [],
                descripcion:[]
              })
              for(const j in categorias){
                if(articulos[i].idarticulo == categorias[j].idarticulo){
                  res[i].categorias.push(categorias[j])
                }
              }

              for(const k in fotos){
                if(articulos[i].idarticulo == fotos[k].idarticulo){
                  res[i].imagename.push(fotos[k])
                }
              }
            }

            for(const j in res){
              for(const k in res[j].categorias){
                for(const l in descripcion){
                  if(res[j].categorias[k].idcatego == descripcion[l].idcatego){
                    res[j].descripcion.push(descripcion[l])
                  }
                }
              }
            }

            console.log("Artículos: ", res);
            result(null, res);
          });
        });
      });
    }
  });
};


Articulos.getArticulo = (idarticulo, result) => {
  sql.query(`SELECT * FROM articulos WHERE idarticulo = ${idarticulo}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};


Articulos.updateArticulo = (id, art, result)=>{
  sql.query(` UPDATE articulos SET nomart = ?, precio = ?, notas=?
                WHERE idarticulo = ?`, [ art.nomart, art.precio, art.notas, id], 
  (err, res) =>  {

      if (err) { 
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }
      for(const i in art.imagename){
        sql.query(`INSERT INTO fotos(imagename,idarticulo)VALUES(?,?)`,
          [art.imagename[i].imagename, id], (errfoto, resfoto) => { 
          if (errfoto) {
            console.log("error: ", errfoto);
            result(errfoto, null);
            return;
          }
        });
      }
      result(null, "Usuario actualizado correctamente");
    }
  );
};


Articulos.deleteArticulo = (id, result) => {
  sql.query("DELETE FROM articulos WHERE idarticulo = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

Articulos.deleteFoto = (foto, result) => {
  console.log('foto',foto)
  sql.query(`DELETE FROM fotos WHERE idfoto = ?`, foto.idfoto, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    var fs = require('fs');
    var filePath = './../fotos/' + foto.imagename; 
    fs.unlinkSync(filePath);

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", foto);
    result(null, res);
  });
};

module.exports = Articulos;