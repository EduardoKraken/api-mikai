const sql = require("../db.js");

// constructor
const Rutas = (rut) => {
  this.idruta     = rut.idruta;
  this.nomruta    = rut.nomruta;
  this.ruta       = rut.ruta;
};

// VARIABLES PARA QUERYS
// Agregar un rutículo
Rutas.rutaAdd = (c, result) => {
  sql.query(`INSERT INTO rutas(nomruta,ruta)VALUES(?,?)`,
    [c.nomruta,c.ruta], (err, resart) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    console.log("Crear Grupo: ", { id: resart.insertId, ...c });
    result(null, { id: resart.insertId, ...c });
  });
};

// Traer todos los artículos
Rutas.rutasAll = (result) => {
  sql.query(`SELECT * FROM rutas;`, (err, rutas) => { 
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Rutas: ", rutas);
    result(null, rutas);
  });
};


Rutas.updateRuta = (id, art, result)=>{
  sql.query(` UPDATE rutas SET nomruta = ?, ruta = ? WHERE idruta = ?`, [ art.nomruta, art.ruta, id], (err, res) =>  {
      if (err) { 
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, "Ruta actualizada correctamente");
    }
  );
};



module.exports = Rutas;