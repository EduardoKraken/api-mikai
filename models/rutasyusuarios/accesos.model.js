const sql = require("../db.js");

// constructor
const Accesos = (acceso)=> {
  this.idacceso      = acceso.idacceso;
  this.id            = acceso.id;
  this.nomuser       = acceso.nomuser;
  this.nomruta       = acceso.nomruta;
  this.ruta          = acceso.ruta;
  this.idruta          = acceso.idruta;
};

// VARIABLES PARA QUERYS
// Agregar un artículo
Accesos.accesosAdd = (c, result) => {
  sql.query(`INSERT INTO accesos(idusuario,idruta)VALUES(?,?)`,[c.idusuario,c.idruta], (err, res) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    console.log("Crear Acceso: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
  });
};

// Traer todos los artículos
Accesos.accesosAll = (result) => {
  sql.query(`SELECT a.idaccesos, r.idruta, r.ruta, r.nomruta, a.idusuario FROM rutas r RIGHT JOIN accesos a ON a.idruta = r.idruta;`, (err, accesos) => { 
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    var res = []
    sql.query(`SELECT * FROM usuarios`, (err, usuarios) => { 
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      
      for(const i in usuarios){
        res.push({
          id: usuarios[i].id,
          nomuser: usuarios[i].nomuser,
          email: usuarios[i].email,
          accesos: [],
        })
        for(const j in accesos){
          if(usuarios[i].id == accesos[j].idusuario){
            res[i].accesos.push(accesos[j])
          }
        }
      }
      console.log("Rutas: ", res);
      result(null, res);
    });   
  });
};


Accesos.deleteAcceso = (id, result) => {
  sql.query("DELETE FROM accesos WHERE idaccesos = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted acceso with id: ", id);
    result(null, res);
  });
};

module.exports = Accesos;