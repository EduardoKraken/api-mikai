const sql = require("../db.js");

// constructor
const Descripcion = (desc)=> {
  this.iddescripcion  = desc.iddescripcion;
  this.descripcion    = desc.descripcion;
  this.precio         = desc.precio;
  this.idcatego       = desc.idcatego;
};

// VARIABLES PARA QUERYS
// Agregar un artículo
Descripcion.descripcionAdd = (c, result) => {
  sql.query(`INSERT INTO descripcion(descripcion, idcatego, precio)VALUES(?,?,?)`,[c.descripcion,c.idcatego,c.precio], (err, res) => { 
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
  });
};

// Traer todos los artículos
Descripcion.descripcionAll = (result) => {
  sql.query(`SELECT d.iddescripcion, d.descripcion, d.idcatego, d.precio, c.idcatego, c.nomcatego FROM descripcion d LEFT JOIN catego c ON d.idcatego = c.idcatego;`,
   (err, res) => { 
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("Artículos: ", res);
    result(null, res);
  });
};


Descripcion.getDescripcion = (iddescripcion, result) => {
  sql.query(`SELECT * FROM descripcion WHERE iddescripcion = ${iddescripcion}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};


Descripcion.updateDescripcion = (id, art, result)=>{
  sql.query(` UPDATE descripcion SET descripcion = ?, idcatego = ?, precio = ?
                WHERE iddescripcion = ?`, [ art.descripcion,art.idcatego,art.precio, id], 
  (err, res) =>  {

      if (err) { 
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }
      // console.log("Se actualizo el user: ", { id: id, ...user });
      result(null, "Usuario actualizado correctamente");
    }
  );
};


Descripcion.deleteDescripcion = (id, result) => {
  sql.query("DELETE FROM descripcion WHERE iddescripcion = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

module.exports = Descripcion;