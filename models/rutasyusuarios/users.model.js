const sql = require("../db.js");

// constructor
const users = function(user) {
  this.nomuser = user.nomuser;
  this.apellidos = user.apellidos;
  this.empresa = user.empresa;
  this.telempresa     = user.telempresa;
  this.telmovil   = user.telmovil;
  this.email   = user.email;
  this.modmaquina    = user.modmaquina;
  this.matserie  = user.matserie;
  this.password     = user.password;
  this.estatus   = user.estatus;
};

// VARIABLES PARA QUERYS

users.login = (loger,result) =>{
  sql.query("SELECT * FROM usuarios WHERE email = ? AND password = ?"
    ,[loger.email, loger.password],(err, res) => {
    if (err) { 
      result(err, null);
      return 
    }
    result(null, res)
  });
};

users.session = (loger,result) =>{
  sql.query("SELECT * FROM usuarios WHERE email = ? AND password = ?"
    ,[loger.email, loger.password],(err, res) => {
    if (err) { 
      console.log("error: ", err); 
      result(err, null);
      return 
    }
    console.log('res',res)
    result(null, res[0])
  });
};

users.getAll = result => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM usuarios;`, (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve(res)
    });
  });
};


users.accesosCarpetas = (id_usuarios) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT
              a.*
              ,c.carpeta AS nombre_carpeta
          FROM mikai.accesos_usuarios_carpetas AS a 
            JOIN admincarpetas.carpetas AS c ON a.id_carpetas = c.idcarpetas
          WHERE 
              a.estatus = 1 
          AND a.deleted = 0
          AND a.id_usuarios = ?`, [id_usuarios], (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};

users.consultaUsuariosActivos = () => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT
                u.id
                ,u.nomuser AS nombre_usuario
                ,u.admin AS nivel_usuario
              FROM mikai.usuarios AS u
            WHERE 
              u.estatus = 1`, (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve(res);
    });
  })
};



users.validaEmail = (email, id) =>{
  return new Promise((resolve, reject) => {
    sql.query("SELECT email FROM usuarios WHERE email = ? AND id <> ?;",[email, id],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve(res[0])
    });
  });
};

users.create = (newUser, result) => {
  return new Promise((resolve, reject) => {
    sql.query("INSERT INTO usuarios(nomuser,password,email,estatus,admin)VALUES(?,?,?,?,?)", [newUser.nomuser,newUser.password,newUser.email,newUser.estatus,newUser.admin], (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, ...newUser })
    });
  });
};

users.cambiaEstatus = (user, result)=>{
  sql.query("UPDATE usuarios SET sesion=? WHERE id = ?", [user.sesion, user.id], (err, res) => {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      result(null, res);
    }
  );
};

users.updateUsuario = (user, result)=>{

  return new Promise((resolve, reject) => {
    sql.query(`UPDATE usuarios SET nomuser = ?, email = ?, password=?, admin=?, estatus = ?  WHERE id = ?`,[ user.nomuser, user.email, user.password, user.admin, user.estatus, user.id], (err, res) => {
      
      if(err){ return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) {
        return reject({ message: 'Usuario no encontrado' }); 
      }

      resolve(res)
    });
  });
};

users.OlvideContra = (data,result) =>{
  sql.query(`SELECT id, email, nomuser FROM usuarios WHERE email = ?`,[data.email], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("email: ", res);
    result(null, res);
  });
};

users.passwordExtra = (user, result)=>{
  sql.query(` UPDATE usuarios SET passextra = ? WHERE id = ?`, [ user.codigo, user.id],(err, res) =>  {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      result(null, "Usuario actualizado correctamente");
    }
  );
};

users.updatexId = (id,user, result)=>{
  sql.query(` UPDATE usuarios SET nomuser = ?, email = ?, password=?, admin=? 
                WHERE id = ?`, [ user.nomuser, user.email, user.password, user.admin, user.id], 
  (err, res) =>  {

      if (err) { 
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }
      // console.log("Se actualizo el user: ", { id: id, ...user });
      result(null, "Usuario actualizado correctamente");
    }
  );
};


users.findById = (userid, result) => {
  console.log("Recibo", userid)
  sql.query(`SELECT * FROM usuarios WHERE id = ${userid}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

users.getxEmail = (data, result) => {
  if(data.nomuser == ''){
    sql.query(`SELECT * FROM usuarios WHERE email = ?`,[data.email], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log("found user: ", res[0]);
        result(null, res[0]);
        return;
      }
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }else{
    sql.query(`SELECT * FROM usuarios WHERE email = ? OR nomuser = ?`,[data.email, data.nomuser], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log("found user: ", res[0]);
        result(null, res[0]);
        return;
      }
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  }
  
};

users.updateById = (id, user, result) => {
  sql.query("UPDATE usuarios SET email = ?, nomuser = ?, admin = ? WHERE id = ?", [user.email, user.nomuser, user.admin, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated user: ", { id: id, ...user });
      result(null, { id: id, ...user });
    }
  );
};

users.remove = (id, result) => {
  sql.query("DELETE FROM usuarios WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

users.removeAll = result => {
  sql.query("DELETE FROM usuarios", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} user`);
    result(null, res);
  });
};

users.activarUsuario = (idusuario, result)=>{
  sql.query( `UPDATE usuarios SET estatus = 2 WHERE id = ?`, [idusuario], (err, res) => {
      if (err) { console.log("error: ", err);  result(null, err); return;  } // SI OCURRE UN ERROR LO RETORNO
      if (res.affectedRows == 0) { result({ kind: "No encontrado" }, null); return; } // SI NO ENCUENTRO EL ID

      console.log("Se actualizo el usuario: ", { idusuario: idusuario});
      result(null, { idusuario: idusuario});
    }
  );
};


users.deleteUsuario = (id, result) => {
  sql.query("DELETE FROM usuarios WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

module.exports = users;