const sql = require("../dbArchivos.js");

// constructor
const carpetas = (carpetas) => {};

carpetas.getCarpetasNivel = (nivel, idcarpeta) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM carpetas WHERE nivel = ? AND idcarpeta = ? AND estatus = 1 AND deleted = 0;`, [nivel, idcarpeta], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve(res)
        });
    })
};

carpetas.getArchivos = (idcarpetas) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT a.* 
                          ,u.nomuser AS creador 
                          ,u2.nomuser AS modifico
                        FROM archivos AS a 
                             JOIN mikai.usuarios AS u  ON a.creador = u.id
                             JOIN mikai.usuarios AS u2 ON a.modifico = u2.id
                    WHERE idcarpetas IN ( ? ) AND a.estatus = 1 AND deleted = 0;`, [idcarpetas], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve(res)
        });
    })
};


carpetas.getCarpetasPrincipales = (id) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        a.idaccesos_usuarios_carpetas AS id_accesos
                        ,a.id_usuarios AS usuario_accesos
                        ,c.*
                    FROM 
                        mikai.accesos_usuarios_carpetas AS a 
                            JOIN admincarpetas.carpetas AS c ON a.id_carpetas = c.idcarpetas
                    WHERE
                        a.id_usuarios = ?
                    AND c.nivel = 1
                    AND c.estatus = 1 
                    AND c.deleted = 0`, [id], (err, res) => {

            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve(res)
        });
    })
};
carpetas.getArchivosPrincipales = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT  a.* 
                            ,u.nomuser AS creador 
                            ,u2.nomuser AS modifico
                    FROM archivos AS a 
                        JOIN mikai.usuarios AS u  ON a.creador = u.id
                        JOIN mikai.usuarios AS u2 ON a.modifico = u2.id
                    WHERE a.idcarpetas = 0 AND a.estatus = 1 AND a.deleted = 0;`, (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve(res)
        });
    })
};


carpetas.getCarpetaExacta = (idcarpetas) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT c2.* FROM carpetas c 
      LEFT JOIN (SELECT * FROM carpetas c ) c2 ON c2.idcarpeta = c.idcarpeta
      WHERE c.idcarpetas = ? AND c.estatus = 1 AND c.deleted = 0;`, [idcarpetas], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve(res)
        });
    })
};

carpetas.agregarCarpeta = (c) => {
    return new Promise((resolve, reject) => {
        sql.query(`INSERT INTO carpetas( carpeta, nivel, idcarpeta )VALUES( ?, ?, ?)`, [c.carpeta, c.nivel, c.idcarpeta], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve({ id: res.insertId, ...c })
        });
    })
};

carpetas.getCarpeta = (idcarpetas) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM carpetas WHERE idcarpetas = ?;`, [idcarpetas], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve(res[0])
        });
    })
};


carpetas.agregarArchivo = (archivo, nombre, extension, idcarpetas, idusuario) => {
    return new Promise((resolve, reject) => {
        sql.query(`INSERT INTO archivos( archivo, nombre, extension, idcarpetas,creador, modifico )VALUES( ?, ?, ?, ?, ?, ?)`, [archivo, nombre, extension, idcarpetas, idusuario, idusuario], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve({ idarchivos: res.insertId, archivo, nombre, extension, idcarpetas })
        });
    })
};

carpetas.carpetasDisponibles = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT c.idcarpetas, c.carpeta 
                  FROM carpetas AS c 
                WHERE c.nivel = 1 AND c.estatus = 1 AND c.deleted = 0  `, (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve(res)
        });
    })
};

carpetas.agregarAcceso = (data) => {
    return new Promise((resolve, reject) => {
        sql.query(`INSERT INTO mikai.accesos_usuarios_carpetas( id_carpetas, id_usuarios )VALUES(?,?)`, [data.idcarpeta, data.idusuario], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve({ message: "El acceso se guardo correctamente." })
        });
    })
};

carpetas.eliminarAcceso = (id) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE mikai.accesos_usuarios_carpetas SET deleted = 1 WHERE idaccesos_usuarios_carpetas = ?`, [id], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve({ message: "El acceso se elimino correctamente." })
        });
    })
};

carpetas.renombrarCarpeta = (data) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE carpetas SET carpeta = ? WHERE idcarpetas = ?`, [data.nombre_carpeta, data.id_carpeta], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve({ message: "El nombre de la carpeta se actualizo correctamente.." })
        });
    })
};

carpetas.eliminarCarpeta = (id) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE carpetas SET deleted = 1, estatus = 0 WHERE idcarpetas = ?`, [id], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve({ message: "La carpeta se elimino correctamente." })
        });
    })
};


carpetas.eliminarArchivo = (id) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE archivos SET deleted = 1, estatus = 0 WHERE idarchivos = ?`, [id], (err, res) => {
            if (err) {
                return reject({ message: err.sqlMessage });
            }
            resolve({ message: "La carpeta se elimino correctamente." })
        });
    })
};


module.exports = carpetas;