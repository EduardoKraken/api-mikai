const sql = require("../db.js");
const sqlFormatos = require("../dbFormatos.js");

// constructor
const Formatos = function(formatos) {
  this.idformato                     = formatos.idformato;
  this.responsable                   = formatos.responsable;
  this.noReporte                     = formatos.noReporte;
  this.consecutivo                   = formatos.consecutivo;
  this.fecha                         = formatos.fecha;
  this.servicioNoConforme            = formatos.servicioNoConforme;
  this.auditoriaCalidad              = formatos.auditoriaCalidad;
  this.quejaCliente                  = formatos.quejaCliente;
  this.satisfaccionCliente           = formatos.satisfaccionCliente;
  this.sistemaAdministracionCalidad  = formatos.sistemaAdministracionCalidad;
  this.fuenteInformacion             = formatos.fuenteInformacion;
  this.descripConformidad            = formatos.descripConformidad;
  this.conclusionEvaluacion          = formatos.conclusionEvaluacion;
  this.correcion                     = formatos.correcion;
  this.reproceso                     = formatos.reproceso;
  this.rechazo                       = formatos.rechazo;
  this.aceptadoBajoConcesion         = formatos.aceptadoBajoConcesion;
  this.descripcorrecion              = formatos.descripcorrecion;
  this.descripreproceso              = formatos.descripreproceso;
  this.descriprechazo                = formatos.descriprechazo;
  this.descripaceptadoBajoConcesion  = formatos.descripaceptadoBajoConcesion;
  this.fechaMetodologia              = formatos.fechaMetodologia;
  this.manoObra                      = formatos.manoObra;
  this.metodo                        = formatos.metodo;
  this.masYEquipo                    = formatos.masYEquipo;
  this.materiales                    = formatos.materiales;
  this.medioAmbiente                 = formatos.medioAmbiente;
  this.participante1                 = formatos.participante1;
  this.participante2                 = formatos.participante2;
  this.participante3                 = formatos.participante3;
  this.descripCausaRaiz              = formatos.descripCausaRaiz;
  this.accionCorrectiva              = formatos.accionCorrectiva;
  this.fechaCompromiso               = formatos.fechaCompromiso;
  this.auditorCalidad                = formatos.auditorCalidad;
  this.seguimiento1por               = formatos.seguimiento1por;
  this.seguimiento1Fecha             = formatos.seguimiento1Fecha;
  this.seguimiento2por               = formatos.seguimiento2por;
  this.seguimiento2Fecha             = formatos.seguimiento2Fecha;
  this.seguimiento3por               = formatos.seguimiento3por;
  this.seguimiento3Fecha             = formatos.seguimiento3Fecha;
  this.seguimiento4por               = formatos.seguimiento4por;
  this.seguimiento4Fecha             = formatos.seguimiento4Fecha;
  this.fechaCumplimientoRevision     = formatos.fechaCumplimientoRevision;
  this.veriAccionCorrectiva          = formatos.veriAccionCorrectiva;
  this.veriEvidenciaObjetiva         = formatos.veriEvidenciaObjetiva;
  this.veriFecha                     = formatos.veriFecha;
  this.veriRquiereCambio             = formatos.veriRquiereCambio;
  this.veriRevaluacion               = formatos.veriRevaluacion;
  this.veriAuditor                   = formatos.veriAuditor;
  this.cierreAuditoria               = formatos.cierreAuditoria;
  this.estatus                       = formatos.estatus;
  this.estadoCA                      = formatos.estadoCA;
};

Formatos.addFormatos = (c, result) => {
	sqlFormatos.query(`INSERT INTO formato SET ?`,[c], (err, res) => {	
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    console.log("Crear formato: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
	});
};
Formatos.agregar_nuevo_formulario = (c, result) => {
	sql.query(`INSERT INTO formato(noReporte,fuenteInformacion,descripConformidad,responsable,disposicion,
                                 descripcorrecion,fechaCompromiso,veriFecha)VALUES(?,?,?,?,?,?,?,?)`,
		                             [c.noReporte,c.fuenteInformacion,c.descripConformidad,c.responsable,
                                  c.disposicion,c.descripcorrecion,c.fechaCompromiso,c.veriFecha], 
    (err, res) => {	
      if (err) {  result(err, null);  return;  }

      console.log("Crear formulario: ", { id: res.insertId, ...c });
      result(null, { id: res.insertId, ...c });
	});
};

Formatos.getFormatosID = (params, result)=>{
	sqlFormatos.query(`SELECT nomcli,calle,numext,colonia,ciudad,estado,cp,telefono,email1,email2,rfc,curp,nomcomer,estatus,numint 
    FROM clientes WHERE idweb=?`, [params.idweb], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};
Formatos.getAllFormatos = result => {
  sqlFormatos.query(`SELECT * FROM formato`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("clientes: ", res);
    result(null, res);
  });
};


Formatos.updateFormatos = (id, n, result) => {
  sqlFormatos.query(` UPDATE formato SET responsable  = ?,noReporte  = ?,consecutivo = ?,fecha = ?,servicioNoConforme = ?,auditoriaCalidad = ?,
    quejaCliente = ?,satisfaccionCliente = ?,sistemaAdministracionCalidad = ?,fuenteInformacion = ?,descripConformidad = ?,
    conclusionEvaluacion = ?,correcion = ?,reproceso = ?,rechazo = ?,aceptadoBajoConcesion = ?,descripcorrecion = ?,descripreproceso = ?,
    descriprechazo = ?,descripaceptadoBajoConcesion = ?,fechaMetodologia = ?,manoObra = ?,metodo = ?,masYEquipo = ?,materiales = ?,
    medioAmbiente = ?,participante1 = ?,participante2 = ?,participante3 = ?,descripCausaRaiz = ?,accionCorrectiva = ?,fechaCompromiso = ?,
    auditorCalidad = ?,seguimiento1por = ?,seguimiento1Fecha = ?,seguimiento2por = ?,seguimiento2Fecha = ?,seguimiento3por = ?,
    seguimiento3Fecha = ?,seguimiento4por = ?,seguimiento4Fecha = ?,fechaCumplimientoRevision = ?,veriAccionCorrectiva = ?,
    veriEvidenciaObjetiva = ?,veriFecha = ?,veriRquiereCambio = ?,veriRevaluacion = ?,veriAuditor = ?,cierreAuditoria = ?,estadoCA=?,
    estatus = ? WHERE idformato = ?`, [n.responsable ,n.noReporte ,n.consecutivo,n.fecha,n.servicioNoConforme,n.auditoriaCalidad,n.quejaCliente,
    n.satisfaccionCliente,n.sistemaAdministracionCalidad,n.fuenteInformacion,n.descripConformidad,n.conclusionEvaluacion,
    n.correcion,n.reproceso,n.rechazo,n.aceptadoBajoConcesion,n.descripcorrecion,n.descripreproceso,n.descriprechazo,
    n.descripaceptadoBajoConcesion,n.fechaMetodologia,n.manoObra,n.metodo,n.masYEquipo,n.materiales,n.medioAmbiente,
    n.participante1,n.participante2,n.participante3,n.descripCausaRaiz,n.accionCorrectiva,n.fechaCompromiso,
    n.auditorCalidad,n.seguimiento1por,n.seguimiento1Fecha,n.seguimiento2por,n.seguimiento2Fecha,n.seguimiento3por,
    n.seguimiento3Fecha,n.seguimiento4por,n.seguimiento4Fecha,n.fechaCumplimientoRevision,n.veriAccionCorrectiva,
    n.veriEvidenciaObjetiva,n.veriFecha,n.veriRquiereCambio,n.veriRevaluacion,n.veriAuditor,n.cierreAuditoria,n.estadoCA,
    n.estatus, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated cli: ", { id: id, ...n });
      result(null, { id: id, ...n });
    }
  );
};
Formatos.updateFormatoConsecutivo = (id, cli, result) => {
  sqlFormatos.query(` UPDATE formato SET consecutivo=? 
                WHERE idformato = ?`, [cli.consecutivo, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated cli: ", { id: id, ...cli });
      result(null, { id: id, ...cli });
    }
  );
};




module.exports = Formatos;